package org.estuardosaban.sistema;
import java.io.InputStream;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.estuardosaban.controladores.ClientesController;
import org.estuardosaban.controladores.ComprasController;
import org.estuardosaban.controladores.DetalleVentasController;
import org.estuardosaban.controladores.LoginController;
import org.estuardosaban.controladores.MenuController;
import org.estuardosaban.controladores.ProductosController;
import org.estuardosaban.controladores.ProveedoresController;
import org.estuardosaban.controladores.TipoProductoController;
import org.estuardosaban.controladores.UsuariosController;
import org.estuardosaban.controladores.VentasController;
import org.estuardosaban.modelos.Clientes;

public class Principal extends Application {
    
    private String PAQUETE_VISTA = "/org/estuardosaban/vistas/";
    private Stage escenario;
    private Scene escena;
    
    @Override
    public void start(Stage escenario) {
        this.escenario = escenario;
        escenario.setTitle("DISTRIBUIDORA AMANECER");
        escenario.setResizable(false);
        escenaLogin();
        escenario.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
    
    public void escenaMenu () {
        try {
          MenuController menu = (MenuController) cambiarEscena ("MenuView.fxml",540,550);
          menu.setEscenario (this);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void escenaProveedores () {
        try {
          ProveedoresController proveedores = (ProveedoresController) cambiarEscena ("ProveedoresView.fxml",700,650);
          proveedores.setEscenario (this);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void escenaClientes () {
        try {
          ClientesController clientes = (ClientesController) cambiarEscena ("ClientesView.fxml",700,600);
          clientes.setEscenario (this);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void escenaUsuarios () {
        try {
          UsuariosController usuarios = (UsuariosController) cambiarEscena ("UsuariosView.fxml",700,600);
          usuarios.setEscenario (this);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void escenaTipoProducto () {
        try {
          TipoProductoController tipoProducto = (TipoProductoController) cambiarEscena ("TipoProductoView.fxml",700,600);
          tipoProducto.setEscenario (this);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void escenaProductos () {
        try {
          ProductosController productos = (ProductosController) cambiarEscena ("ProductosView.fxml",780,600);
          productos.setEscenario (this);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void escenaCompras () {
        try {
          ComprasController compras = (ComprasController) cambiarEscena ("ComprasView.fxml",700,600);
          compras.setEscenario (this);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void escenaVentas () {
        try {
          VentasController ventas = (VentasController) cambiarEscena ("VentasView.fxml",872,600);
          ventas.setEscenario (this);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void escenaDetalleVentas () {
        try {
          DetalleVentasController detalleVentas = (DetalleVentasController) cambiarEscena ("DetalleVentasView.fxml",700,600);
          detalleVentas.setEscenario (this);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void escenaLogin () {
        try {
          LoginController login = (LoginController) cambiarEscena ("LoginView.fxml",500,430);
          login.setEscenario (this);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public Initializable cambiarEscena (String fxml,int ancho,int alto) throws Exception {
        Initializable resultado = null;
        FXMLLoader cargadorFXML = new FXMLLoader();
        InputStream archivo = Principal.class.getResourceAsStream(PAQUETE_VISTA + fxml);
        cargadorFXML.setBuilderFactory(new JavaFXBuilderFactory());
        cargadorFXML.setLocation(Principal.class.getResource(PAQUETE_VISTA + fxml));
        escena = new Scene((AnchorPane) cargadorFXML.load(archivo),ancho,alto);
        escenario.setScene(escena);
        escenario.sizeToScene();
        resultado = (Initializable)cargadorFXML.getController();
        return resultado;
    }
    
}
