package org.estuardosaban.controladores;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.StringConverter;
import javax.swing.JOptionPane;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.control.Label;

import org.estuardosaban.db.Conexion;
import org.estuardosaban.sistema.Principal;

import org.estuardosaban.modelos.Ventas;
import org.estuardosaban.modelos.Clientes;
import org.estuardosaban.modelos.Productos;
import org.estuardosaban.modelos.TipoProducto;
import org.estuardosaban.modelos.FormaPago;
import org.estuardosaban.controladores.LoginController;

public class VentasController implements Initializable {
    private enum operaciones{NUEVO, GUARDAR, ELIMINAR, ACTUALIZAR, NINGUNO}
    private Principal escenario;
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<FormaPago> listaFormaPago;
    private ObservableList<TipoProducto> listaTipoProducto;
    private ObservableList<Clientes> listaClientes;
    private ObservableList<Ventas> listaVentas;
    
    @FXML private ComboBox cmbCliente;
    @FXML private ComboBox cmbFormaPago;
    @FXML private ComboBox cmbTipoProducto;
    @FXML private TextField txtObservacion;
    @FXML private Label txtUsuario;
    @FXML private TextField txtNumeroFactura;
    
    @FXML private TableView tblVentas;
    @FXML private TableColumn colCodigo;
    @FXML private TableColumn colFechaVenta;
    @FXML private TableColumn colProductoIVA;
    @FXML private TableColumn colObservacion;
    @FXML private TableColumn colFechaCreacion;
    @FXML private TableColumn colFechaModificacion;
    @FXML private TableColumn colCliente;
    @FXML private TableColumn colTipoProducto;
    @FXML private TableColumn colFormaPago;
    @FXML private TableColumn colUsuario;
    @FXML private TableColumn colNoFactura;
    
    @FXML private Button btnRegresar;
    @FXML private Button btnNuevo;
    @FXML private Button btnEditar;
    @FXML private Button btnEliminar;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
        desactivarControles();
        txtUsuario.setText(LoginController.nombre + " " + LoginController.apellido);
    }
    public void cargarDatos(){
        tblVentas.setItems(getVentas());
        colCodigo.setCellValueFactory(new PropertyValueFactory<Ventas, Integer>("codigoVenta"));
        colFechaVenta.setCellValueFactory(new PropertyValueFactory<Ventas, Integer>("fechaVenta"));
        colProductoIVA.setCellValueFactory(new PropertyValueFactory<Ventas, Integer>("iva"));
        colObservacion.setCellValueFactory(new PropertyValueFactory<Ventas, String>("observacion"));
        colFechaCreacion.setCellValueFactory(new PropertyValueFactory<Ventas, Double>("fechaCreacion"));
        colFechaModificacion.setCellValueFactory(new PropertyValueFactory<Ventas, Integer>("fechaModificacion"));
        colCliente.setCellValueFactory(new PropertyValueFactory<Ventas, Double>("codigoCliente"));
        colFormaPago.setCellValueFactory(new PropertyValueFactory<Ventas, Double>("codigoFormaPago"));
        colTipoProducto.setCellValueFactory(new PropertyValueFactory<Ventas, Double>("codigoTipoProducto"));
        colUsuario.setCellValueFactory(new PropertyValueFactory<Ventas, Integer>("codigoUsuario"));
        colNoFactura.setCellValueFactory(new PropertyValueFactory<Ventas, Integer>("numeroFactura"));
        cmbCliente.setItems(getClientes());
        cmbTipoProducto.setItems(getTipoProducto());
        cmbFormaPago.setItems(getFormaPago());
    }
    public ObservableList<Ventas> getVentas() {
        ArrayList<Ventas> lista = new ArrayList<Ventas>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarVentas}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Ventas(resultado.getInt("codigoVenta"), resultado.getDate("fechaVenta"), resultado.getInt("iva"), resultado.getString("observacion"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion"), resultado.getDouble("total"), resultado.getInt("numeroFactura"), resultado.getInt("codigoCliente"), resultado.getInt("codigoFormaPago"), resultado.getInt("codigoTipoProducto"), resultado.getInt("codigoUsuario")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaVentas = FXCollections.observableArrayList(lista);
    }
    public Ventas buscarVenta(int codVenta){
        Ventas resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarVentas(?)}");
            procedimiento.setInt(1, codVenta);
            ResultSet registro= procedimiento.executeQuery();
            while (registro.next()){
                resultado = new Ventas(registro.getInt("codigoVenta"), registro.getDate("fechaVenta"), registro.getInt("iva"), registro.getString("observacion"), registro.getDate("fechaCreacion"), registro.getDate("fechaModificacion"), registro.getDouble("total"), registro.getInt("numeroFactura"), registro.getInt("codigoCliente"), registro.getInt("codigoFormaPago"), registro.getInt("codigoTipoProducto"), registro.getInt("codigoUsuario"));  
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    public ObservableList<Clientes> getClientes(){
        ArrayList<Clientes> lista = new ArrayList<Clientes>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarClientes}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new Clientes (resultado.getInt("codigoCliente"),resultado.getString("nombreCliente"),resultado.getString("apellidoCliente"),resultado.getString("direccionCliente"),resultado.getString("nitCliente"),resultado.getDate("fechaCreacion"),resultado.getDate("fechaModificacion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaClientes = FXCollections.observableArrayList(lista);
    }
    public Clientes buscarCliente(int codCliente){
        Clientes resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarClientes(?)}");
            procedimiento.setInt(1, codCliente);
            ResultSet registro = procedimiento.executeQuery();
            while(registro.next()){
                resultado = new Clientes(registro.getInt("codigoCliente"),registro.getString("nombreCliente"),registro.getString("apellidoCliente"),registro.getString("direccionCliente"),registro.getString("nitCliente"),registro.getDate("fechaCreacion"),registro.getDate("fechaModificacion"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    public ObservableList<TipoProducto> getTipoProducto(){
        ArrayList<TipoProducto> lista = new ArrayList<TipoProducto>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarTipoProducto}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new TipoProducto (resultado.getInt("codigoTipoProducto"),resultado.getString("descripcion"),resultado.getDate("fechaCreacion"),resultado.getDate("fechaModificacion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaTipoProducto = FXCollections.observableArrayList(lista);
    } 
    public TipoProducto buscarTipoProducto(int codTipoProducto){
        TipoProducto resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarTipoProducto(?)}");
            procedimiento.setInt(1, codTipoProducto);
            ResultSet registro = procedimiento.executeQuery();
            while(registro.next()){
                resultado = new TipoProducto(registro.getInt("codigoTipoProducto"),registro.getString("descripcion"),registro.getDate("fechaCreacion"),registro.getDate("fechaModificacion"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    public ObservableList<FormaPago> getFormaPago(){
        ArrayList<FormaPago> lista = new ArrayList<FormaPago>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarFormaPago}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new FormaPago (resultado.getInt("codigoFormaPago"),resultado.getString("descripcion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaFormaPago = FXCollections.observableArrayList(lista);
    } 
    public FormaPago buscarFormaPago(int codFormaPago){
        FormaPago resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarFormaPago(?)}");
            procedimiento.setInt(1, codFormaPago);
            ResultSet registro = procedimiento.executeQuery();
            while(registro.next()){
                resultado = new FormaPago(registro.getInt("codigoFormaPago"),registro.getString("descripcion"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    public void seleccionarElementos(){
        if(tblVentas.getSelectionModel().getSelectedIndex()>-1){
            txtObservacion.setText(String.valueOf(((Ventas)tblVentas.getSelectionModel().getSelectedItem()).getObservacion()));
            cmbCliente.getSelectionModel().select(buscarCliente(((Ventas)tblVentas.getSelectionModel().getSelectedItem()).getCodigoCliente()));
            cmbFormaPago.getSelectionModel().select(buscarFormaPago(((Ventas)tblVentas.getSelectionModel().getSelectedItem()).getCodigoFormaPago()));
            cmbTipoProducto.getSelectionModel().select(buscarTipoProducto(((Ventas)tblVentas.getSelectionModel().getSelectedItem()).getCodigoTipoProducto()));
            txtUsuario.setText(String.valueOf(LoginController.nombre + " " + LoginController.apellido));
            txtNumeroFactura.setText(String.valueOf(((Ventas)tblVentas.getSelectionModel().getSelectedItem()).getNumeroFactura()));
        }
    }
    boolean ward = true;
    public void nuevo(){
        switch(tipoOperaciones){
            case NINGUNO:
                activarControles();
                limpiarControles();
                btnNuevo.setText("GUARDAR");
                btnEliminar.setText("CANCELAR");
                btnEditar.setDisable(true);
                tipoOperaciones=operaciones.GUARDAR;
            break;
            case GUARDAR:
                agregar();
                if(ward == true){
                    btnNuevo.setText("NUEVO");
                    btnEliminar.setText("ELIMINAR");
                    btnEditar.setDisable(false);
                    tipoOperaciones=operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    }
    public void editar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblVentas.getSelectionModel().getSelectedItem() != null){
                    btnEditar.setText("ACTUALIZAR");
                    btnEliminar.setText("CANCELAR");
                    btnNuevo.setDisable(true);
                    tipoOperaciones = operaciones.ACTUALIZAR;
                    txtObservacion.setEditable(true);
                    cmbCliente.setDisable(false);
                    cmbTipoProducto.setDisable(false);
                    cmbFormaPago.setDisable(false);
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar una Venta");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(ward == true){
                    btnEditar.setText("EDITAR");
                    btnEliminar.setText("ELIMINAR");
                    btnNuevo.setDisable(false);
                    tipoOperaciones = operaciones.NINGUNO;
                    txtObservacion.setEditable(true);
                    cmbCliente.setDisable(true);
                    cmbTipoProducto.setDisable(true);
                    cmbFormaPago.setDisable(true);
                    cargarDatos();
                }
            break;
        }
    }
    public void eliminar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblVentas.getSelectionModel().getSelectedItem() != null){
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Venta",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION){
                        borrar();
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar una Venta");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    public void activarControles(){
        txtObservacion.setEditable(true); 
        txtNumeroFactura.setEditable(true);
        cmbCliente.setDisable(false);
        cmbTipoProducto.setDisable(false);
        cmbFormaPago.setDisable(false);
    }
    
    public void desactivarControles(){
        txtObservacion.setEditable(false);
        txtNumeroFactura.setEditable(false);
        cmbCliente.setDisable(true);
        cmbTipoProducto.setDisable(true);
        cmbFormaPago.setDisable(true);
    }
    
    public void limpiarControles(){
        txtObservacion.setText("");
        txtNumeroFactura.setText("");
        cmbCliente.setValue("");
        cmbTipoProducto.setValue("");
        cmbFormaPago.setValue("");
    }
    
    public void agregar(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        try{
            if(!(txtObservacion.getText().equals(""))&&!(cmbCliente.getValue().equals(""))&&!(cmbTipoProducto.getValue().equals(""))&&!(cmbFormaPago.getValue().equals(""))){
                Ventas registro = new Ventas();            
                registro.setObservacion(txtObservacion.getText());
                registro.setCodigoCliente(((Clientes)cmbCliente.getSelectionModel().getSelectedItem()).getCodigoCliente());               
                registro.setCodigoTipoProducto(((TipoProducto)cmbTipoProducto.getSelectionModel().getSelectedItem()).getCodigoTipoProducto());
                registro.setCodigoFormaPago(((FormaPago)cmbFormaPago.getSelectionModel().getSelectedItem()).getCodigoFormaPago());
                registro.setCodigoUsuario(LoginController.codigoUsuario);
                try{
                    registro.setNumeroFactura(Integer.parseInt(txtNumeroFactura.getText()));
                }catch(Exception e){
                    JOptionPane.showMessageDialog(null, "Ingrese Un Numero de Factura Correcto");
                }
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_InsertarVentas(?,?,?,?,?,?,?,?,?)}");
                procedimiento.setString(1, dateFormat.format(date));
                procedimiento.setDouble(2, 0.12);
                procedimiento.setString(3, registro.getObservacion());
                procedimiento.setString(4, dateFormat.format(date));
                procedimiento.setInt(5, registro.getCodigoCliente());
                procedimiento.setInt(6, registro.getCodigoFormaPago());
                procedimiento.setInt(7, registro.getCodigoTipoProducto());
                procedimiento.setDouble(8, registro.getCodigoUsuario());
                procedimiento.setInt(9, registro.getNumeroFactura());
                procedimiento.execute();
                listaVentas.add(registro);
                ward = true;
            }else{
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                ward = false;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    public void actualizar(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        try{
            if(!(txtObservacion.getText().equals(""))&&!(cmbCliente.getValue().equals(""))&&!(cmbTipoProducto.getValue().equals(""))&&!(cmbFormaPago.getValue().equals(""))){
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ModificarVentas(?,?,?,?,?,?,?,?,?,?)}");
                Ventas registro = (Ventas)tblVentas.getSelectionModel().getSelectedItem();
                registro.setObservacion(txtObservacion.getText());
                registro.setCodigoCliente(((Clientes)cmbCliente.getSelectionModel().getSelectedItem()).getCodigoCliente());               
                registro.setCodigoTipoProducto(((TipoProducto)cmbTipoProducto.getSelectionModel().getSelectedItem()).getCodigoTipoProducto());
                registro.setCodigoFormaPago(((FormaPago)cmbFormaPago.getSelectionModel().getSelectedItem()).getCodigoFormaPago());
                registro.setCodigoUsuario(LoginController.codigoUsuario);
                try{
                    registro.setNumeroFactura(Integer.parseInt(txtNumeroFactura.getText()));
                }catch(Exception e){
                    JOptionPane.showMessageDialog(null, "Ingrese Un Numero de Factura Correcto");
                }
                procedimiento.setInt(1, registro.getCodigoVenta());
                procedimiento.setString(2, dateFormat.format(date));
                procedimiento.setDouble(3, 0.12);
                procedimiento.setString(4, registro.getObservacion());
                procedimiento.setString(5, dateFormat.format(date));
                procedimiento.setInt(6, registro.getCodigoCliente());
                procedimiento.setInt(7, registro.getCodigoFormaPago());
                procedimiento.setInt(8, registro.getCodigoTipoProducto());
                procedimiento.setDouble(9, registro.getCodigoUsuario());
                procedimiento.setInt(10, registro.getNumeroFactura());
                procedimiento.execute();
                ward = true;
            }else{
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                ward = false;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    public void borrar(){
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_EliminarVentas(?)}");
            procedimiento.setInt(1, ((Ventas)tblVentas.getSelectionModel().getSelectedItem()).getCodigoVenta());
            procedimiento.execute();
            listaVentas.remove(tblVentas.getSelectionModel().getSelectedIndex());
            limpiarControles();
            cancelar();
            tipoOperaciones = operaciones.NINGUNO;
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    public void cancelar(){
        escenario.escenaVentas();
    }

    public Principal getEscenario() {
        return escenario;
    }

    public void setEscenario(Principal escenario) {
        this.escenario = escenario;
    }
    
    public void escenaMenu(){
        this.escenario.escenaMenu();
    }
    
    public void escenaDetalleVentas(){
        this.escenario.escenaDetalleVentas();
    }
    
    
}
