package org.estuardosaban.controladores;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import org.estuardosaban.sistema.Principal;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.JOptionPane;
import org.estuardosaban.modelos.Proveedores;
import org.estuardosaban.db.Conexion;

public class ProveedoresController implements Initializable {
    
    private enum operaciones{NUEVO, GUARDAR, ELIMINAR, ACTUALIZAR, NINGUNO}
    private Principal escenario;
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<Proveedores>listaProveedores;    
    @FXML private TextField txtRazonSocial;
    @FXML private TextField txtNit;
    @FXML private TextField txtDireccion;
    @FXML private TextField txtPaginaWeb;
    @FXML private TableView tblProveedores;
    @FXML private TableColumn colCodigo;
    @FXML private TableColumn colRazonSocial;
    @FXML private TableColumn colNit;
    @FXML private TableColumn colDireccion;
    @FXML private TableColumn colPaginaWeb;
    @FXML private Button btnNuevo;
    @FXML private Button btnEditar;
    @FXML private Button btnEliminar;
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
    }
    
    public void cargarDatos(){
        tblProveedores.setItems(getProveedores());
        colCodigo.setCellValueFactory(new PropertyValueFactory<Proveedores, Integer>("codigoProveedor"));
        colRazonSocial.setCellValueFactory(new PropertyValueFactory<Proveedores, String>("razonSocial"));
        colNit.setCellValueFactory(new PropertyValueFactory<Proveedores, String>("nit"));
        colDireccion.setCellValueFactory(new PropertyValueFactory<Proveedores, String>("direccionProveedor"));
        colPaginaWeb.setCellValueFactory(new PropertyValueFactory<Proveedores, String>("paginaWeb"));
        txtRazonSocial.setEditable(false);
        txtNit.setEditable(false);
        txtDireccion.setEditable(false);
        txtPaginaWeb.setEditable(false);
        txtNit.setEditable(false);
        txtRazonSocial.setEditable(false);
    }
    
    public ObservableList<Proveedores> getProveedores(){
        ArrayList<Proveedores> lista = new ArrayList<Proveedores>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarProveedores}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new Proveedores (resultado.getInt("codigoProveedor"),resultado.getString("razonSocial"),resultado.getString("nit"),resultado.getString("direccionProveedor"),resultado.getString("paginaWeb")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaProveedores = FXCollections.observableArrayList(lista);
    }
    
    
    public void seleccionarElementos(){
        if(tblProveedores.getSelectionModel().getSelectedIndex()>-1){
            txtRazonSocial.setText(((Proveedores)tblProveedores.getSelectionModel().getSelectedItem()).getRazonSocial());
            txtNit.setText(((Proveedores)tblProveedores.getSelectionModel().getSelectedItem()).getNit());
            txtDireccion.setText(((Proveedores)tblProveedores.getSelectionModel().getSelectedItem()).getDireccionProveedor());
            txtPaginaWeb.setText(((Proveedores)tblProveedores.getSelectionModel().getSelectedItem()).getPaginaWeb());
        }
    }
    
    public Proveedores buscarProveedor(int codProveedor){
        Proveedores resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarProveedores(?)}");
            procedimiento.setInt(1, codProveedor);
            ResultSet registro = procedimiento.executeQuery();
            while(registro.next()){
                resultado = new Proveedores(registro.getInt("codigoProveedor"),registro.getString("razonSocial"),registro.getString("nit"),registro.getString("direccionProveedor"),registro.getString("paginaWeb"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    
    boolean ward = true;
    public void nuevo(){
        switch(tipoOperaciones){
            case NINGUNO:
                activarControles();
                limpiarControles();
                btnNuevo.setText("GUARDAR");
                btnEliminar.setText("CANCELAR");
                btnEditar.setDisable(true);
                tipoOperaciones=operaciones.GUARDAR;
            break;
            case GUARDAR:
                agregar();
                if(ward == true){
                    btnNuevo.setText("NUEVO");
                    btnEliminar.setText("ELIMINAR");
                    btnEditar.setDisable(false);
                    tipoOperaciones=operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    }
    
    public void editar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblProveedores.getSelectionModel().getSelectedItem() != null){
                    btnEditar.setText("ACTUALIZAR");
                    btnEliminar.setText("CANCELAR");
                    btnNuevo.setDisable(true);
                    tipoOperaciones = operaciones.ACTUALIZAR;
                    txtRazonSocial.setEditable(true);
                    txtNit.setEditable(true);
                    txtDireccion.setEditable(true);
                    txtPaginaWeb.setEditable(true);
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Proveedor");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(ward == true){
                    btnEditar.setText("EDITAR");
                    btnEliminar.setText("ELIMINAR");
                    btnNuevo.setDisable(false);
                    txtRazonSocial.setEditable(false);
                    txtNit.setEditable(false);
                    txtDireccion.setEditable(false);
                    txtPaginaWeb.setEditable(false);
                    tipoOperaciones = operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    }
    
    public void eliminar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblProveedores.getSelectionModel().getSelectedItem() != null){
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Proveedor",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION){
                        borrar();
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Proveedor");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void agregar(){
        boolean entrar = true;
        try{
            if(!(txtRazonSocial.getText().equals(""))&&!(txtNit.getText().equals(""))&&!(txtDireccion.getText().equals(""))&&!(txtPaginaWeb.getText().equals(""))&&(entrar==true)){
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_InsertarProveedores(?,?,?,?)}");
                Proveedores registro = new Proveedores();
                registro.setRazonSocial(txtRazonSocial.getText());
                registro.setNit(txtNit.getText());
                registro.setDireccionProveedor(txtDireccion.getText());
                registro.setPaginaWeb(txtPaginaWeb.getText());
                procedimiento.setString(1, registro.getRazonSocial());
                procedimiento.setString(2, registro.getNit());
                procedimiento.setString(3, registro.getDireccionProveedor());
                procedimiento.setString(4, registro.getPaginaWeb());
                procedimiento.execute();
                listaProveedores.add(registro);
                ward = true;
            }else{
                if(entrar == true){
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    ward = false;
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void actualizar(){
        boolean entrar = true;
        try{
            if(!(txtRazonSocial.getText().equals(""))&&!(txtNit.getText().equals(""))&&!(txtDireccion.getText().equals(""))&&!(txtPaginaWeb.getText().equals(""))&&(entrar==true)){
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ModificarProveedores(?,?,?,?,?)}");
                Proveedores registro = (Proveedores)tblProveedores.getSelectionModel().getSelectedItem();
                registro.setRazonSocial(txtRazonSocial.getText());
                registro.setNit(txtNit.getText());
                registro.setDireccionProveedor(txtDireccion.getText());
                registro.setPaginaWeb(txtPaginaWeb.getText());
                procedimiento.setInt(1, registro.getCodigoProveedor());
                procedimiento.setString(2, registro.getRazonSocial());
                procedimiento.setString(3, registro.getNit());
                procedimiento.setString(4, registro.getDireccionProveedor());
                procedimiento.setString(5, registro.getPaginaWeb());
                procedimiento.execute();
                ward = true;
            }else{
                if(entrar == true){
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    ward = false;
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void borrar(){
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_EliminarProveedores(?)}");
            procedimiento.setInt(1, ((Proveedores)tblProveedores.getSelectionModel().getSelectedItem()).getCodigoProveedor());
            procedimiento.execute();
            listaProveedores.remove(tblProveedores.getSelectionModel().getSelectedIndex());
            limpiarControles();
            cargarDatos();
            tipoOperaciones = operaciones.NINGUNO;
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void activarControles(){
        txtRazonSocial.setEditable(true);
        txtNit.setEditable(true);
        txtDireccion.setEditable(true);
        txtPaginaWeb.setEditable(true);
    }
    
    public void desactivarControles(){
        txtRazonSocial.setEditable(false);
        txtNit.setEditable(false);
        txtDireccion.setEditable(false);
        txtPaginaWeb.setEditable(false);
    }
    
    public void limpiarControles(){
        txtRazonSocial.setText("");
        txtNit.setText("");
        txtDireccion.setText("");
        txtPaginaWeb.setText("");
    }
    
    public void cancelar(){
        escenario.escenaProveedores();
    }    
    
    public Principal getEscenario() {
        return escenario;
    }

    public void setEscenario(Principal escenario) {
        this.escenario = escenario;
    }
    
    public void escenaMenu() {
        this.escenario.escenaMenu();
    }
    
    public void ventanaTelProveedores(){
        //codigo=((Proveedor)tblProveedores.getSelectionModel().getSelectedItem()).getCodProveedor();
        //escenario.ventanaTelProveedores();
    }
    
    public void ventanaEmailProveedores(){
        //escenario.ventanaEmailProveedores();
    }
}
