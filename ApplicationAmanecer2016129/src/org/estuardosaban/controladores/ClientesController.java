package org.estuardosaban.controladores;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import org.estuardosaban.sistema.Principal;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.JOptionPane;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import javafx.scene.image.ImageView;
import org.estuardosaban.modelos.Clientes;
import org.estuardosaban.db.Conexion;
import org.estuardosaban.modelos.Proveedores;


public class ClientesController implements Initializable {
 
    private enum operaciones{NUEVO, GUARDAR, ELIMINAR, ACTUALIZAR, NINGUNO}
    private Principal escenario;
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<Clientes>listaClientes;    
    @FXML private TextField txtNombre;
    @FXML private TextField txtApellido;
    @FXML private TextField txtDireccion;
    @FXML private TextField txtNit;
    @FXML private TableView tblClientes;
    @FXML private TableColumn colCodigo;
    @FXML private TableColumn colNombre;
    @FXML private TableColumn colApellido;
    @FXML private TableColumn colDireccion;
    @FXML private TableColumn colNit;
    @FXML private TableColumn colFechaCreacion;
    @FXML private TableColumn colFechaModificacion;
    @FXML private ImageView imagen;
    @FXML private Button btnNuevo;
    @FXML private Button btnEditar;
    @FXML private Button btnEliminar;
    @FXML private Button btnRegresar;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
    }
    
    public void cargarDatos(){
        tblClientes.setItems(getClientes());
        colCodigo.setCellValueFactory(new PropertyValueFactory<Clientes, Integer>("codigoCliente"));
        colNombre.setCellValueFactory(new PropertyValueFactory<Clientes, String>("nombreCliente"));
        colApellido.setCellValueFactory(new PropertyValueFactory<Clientes, String>("apellidoCliente"));
        colDireccion.setCellValueFactory(new PropertyValueFactory<Clientes, String>("direccionCliente"));
        colNit.setCellValueFactory(new PropertyValueFactory<Clientes, String>("nitCliente"));
        colFechaCreacion.setCellValueFactory(new PropertyValueFactory<Clientes, Date>("fechaCreacion"));
        colFechaModificacion.setCellValueFactory(new PropertyValueFactory<Clientes, Date>("fechaModificacion"));
        txtNombre.setEditable(false);
        txtApellido.setEditable(false);
        txtDireccion.setEditable(false);
        txtNit.setEditable(false);
    }
    
    public ObservableList<Clientes> getClientes(){
        ArrayList<Clientes> lista = new ArrayList<Clientes>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarClientes}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new Clientes (resultado.getInt("codigoCliente"),resultado.getString("nombreCliente"),resultado.getString("apellidoCliente"),resultado.getString("direccionCliente"),resultado.getString("nitCliente"),resultado.getDate("fechaCreacion"),resultado.getDate("fechaModificacion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaClientes = FXCollections.observableArrayList(lista);
    }
        
    public void seleccionarElementos(){
        if(tblClientes.getSelectionModel().getSelectedIndex()>-1){
            txtNombre.setText(((Clientes)tblClientes.getSelectionModel().getSelectedItem()).getNombreCliente());
            txtApellido.setText(((Clientes)tblClientes.getSelectionModel().getSelectedItem()).getApellidoCliente());
            txtDireccion.setText(((Clientes)tblClientes.getSelectionModel().getSelectedItem()).getDireccionCliente());
            txtNit.setText(((Clientes)tblClientes.getSelectionModel().getSelectedItem()).getNitCliente());
        }
    }
    
    public Clientes buscarCliente(int codCliente){
        Clientes resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarClientes(?)}");
            procedimiento.setInt(1, codCliente);
            ResultSet registro = procedimiento.executeQuery();
            while(registro.next()){
                resultado = new Clientes(registro.getInt("codigoCliente"),registro.getString("nombreCliente"),registro.getString("apellidoCliente"),registro.getString("direccionCliente"),registro.getString("nitCliente"),registro.getDate("fechaCreacion"),registro.getDate("fechaModificacion"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    boolean ward = true;
    public void nuevo(){
        switch(tipoOperaciones){
            case NINGUNO:
                activarControles();
                limpiarControles();
                btnNuevo.setText("GUARDAR");
                btnEliminar.setText("CANCELAR");
                btnEditar.setDisable(true);
                tipoOperaciones=ClientesController.operaciones.GUARDAR;
            break;
            case GUARDAR:
                agregar();
                if(ward == true){
                    btnNuevo.setText("NUEVO");
                    btnEliminar.setText("ELIMINAR");
                    btnEditar.setDisable(false);
                    tipoOperaciones=ClientesController.operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    }
    
    public void editar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblClientes.getSelectionModel().getSelectedItem() != null){
                    btnEditar.setText("ACTUALIZAR");
                    btnEliminar.setText("CANCELAR");
                    btnNuevo.setDisable(true);
                    tipoOperaciones = ClientesController.operaciones.ACTUALIZAR;
                    txtNombre.setEditable(true);
                    txtApellido.setEditable(true);
                    txtDireccion.setEditable(true);
                    txtNit.setEditable(true);
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Cliente");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(ward == true){
                    btnEditar.setText("EDITAR");
                    btnEliminar.setText("ELIMINAR");
                    btnNuevo.setDisable(false);
                    txtNombre.setEditable(false);
                    txtApellido.setEditable(false);
                    txtDireccion.setEditable(false);
                    txtNit.setEditable(false);
                    tipoOperaciones = ClientesController.operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    }
    
    public void eliminar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblClientes.getSelectionModel().getSelectedItem() != null){
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Cliente",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION){
                        borrar();
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Cliente");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void agregar(){
        boolean entrar = true;
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String fechaMod = "Sin Modificar";
        try{
            if(!(txtNombre.getText().equals(""))&&!(txtApellido.getText().equals(""))&&!(txtDireccion.getText().equals(""))&&!(txtNit.getText().equals(""))&&(entrar==true)){
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_InsertarClientes(?,?,?,?,?)}");
                Clientes registro = new Clientes();
                registro.setNombreCliente(txtNombre.getText());
                registro.setApellidoCliente(txtApellido.getText());
                registro.setDireccionCliente(txtDireccion.getText());
                registro.setNitCliente(txtNit.getText());
                procedimiento.setString(1, registro.getNombreCliente());
                procedimiento.setString(2, registro.getApellidoCliente());
                procedimiento.setString(3, registro.getDireccionCliente());
                procedimiento.setString(4, registro.getNitCliente());
                procedimiento.setString(5, dateFormat.format(date));
                procedimiento.execute();
                listaClientes.add(registro);
                cargarDatos();
                ward = true;
            }else{
                if(entrar == true){
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    ward = false;
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
        public void actualizar(){
           DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
           Date date = new Date();
        try{
            if(!(txtNombre.getText().equals(""))&&!(txtApellido.getText().equals(""))&&!(txtDireccion.getText().equals("")&&!(txtNit.getText().equals("")))){
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ModificarClientes(?,?,?,?,?,?)}");
                Clientes registro = (Clientes)tblClientes.getSelectionModel().getSelectedItem();
                registro.setNombreCliente(txtNombre.getText());
                registro.setApellidoCliente(txtApellido.getText());
                registro.setDireccionCliente(txtDireccion.getText());
                registro.setNitCliente(txtNit.getText());
                procedimiento.setInt(1, registro.getCodigoCliente());
                procedimiento.setString(2, registro.getNombreCliente());
                procedimiento.setString(3, registro.getApellidoCliente());
                procedimiento.setString(4, registro.getDireccionCliente());
                procedimiento.setString(5, registro.getNitCliente());
                procedimiento.setString(6, dateFormat.format(date));
                procedimiento.execute();
                ward = true;
            }else{
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                ward = false;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
     
    public void borrar(){
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_EliminarClientes(?)}");
            procedimiento.setInt(1, ((Clientes)tblClientes.getSelectionModel().getSelectedItem()).getCodigoCliente());
            procedimiento.execute();
            listaClientes.remove(tblClientes.getSelectionModel().getSelectedIndex());
            limpiarControles();
            cargarDatos();
            tipoOperaciones = operaciones.NINGUNO;
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void limpiarControles(){
        txtNombre.setText("");
        txtApellido.setText("");
        txtDireccion.setText("");
        txtNit.setText("");
    }
    
    public void activarControles(){
        txtNombre.setEditable(true);
        txtApellido.setEditable(true);
        txtDireccion.setEditable(true);
        txtNit.setEditable(true);
    }
    
    public void desactivarControles(){
        txtNombre.setEditable(false);
        txtDireccion.setEditable(false);
        txtNit.setEditable(false);
        txtNit.setEditable(false);
    }
    
    
    public void cancelar(){
        escenario.escenaClientes();
    }

    public Principal getEscenario() {
        return escenario;
    }

    public void setEscenario(Principal escenario) {
        this.escenario = escenario;
    }
    
    public void escenaMenu(){
        this.escenario.escenaMenu();
    }
    
    public void ventanaTelClientes(){
        //escenario.escenaTelefonoClientes();
    }
    
    public void ventanaEmailClientes(){
        //escenarioPrincipal.ventanaEmailClientes();
    }
}
