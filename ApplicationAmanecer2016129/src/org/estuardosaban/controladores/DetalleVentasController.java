package org.estuardosaban.controladores;
import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javax.swing.JOptionPane;
import org.estuardosaban.modelos.Clientes;
import org.estuardosaban.modelos.DetalleVentas;
import org.estuardosaban.modelos.Productos;
import org.estuardosaban.modelos.Ventas;
import org.estuardosaban.db.Conexion;
import org.estuardosaban.sistema.Principal;

public class DetalleVentasController implements Initializable {

    private Principal escenario;
    private enum operaciones {AGREGAR, ACTUALIZAR, NINGUNO}
    private operaciones operacionActual = operaciones.NINGUNO;
    private ObservableList<DetalleVentas> listaDetallesVenta;
    private ObservableList<Ventas> listaVentas;
    private ObservableList<Productos> listaProductos;
    private ObservableList<Clientes> listaClientes;
    private boolean camposCorrectos;
    private int codigoVenta;
    private int codigoTipoProducto;
    @FXML private TextField txtCantidad;
    @FXML private TextField txtPrecioVenta;
    @FXML private TextField txtProductoIVA;
    @FXML private ComboBox cmbVenta;
    @FXML private ComboBox cmbCodigoProducto;
    @FXML private TextField txtTotal;
    
    @FXML private TableView tblDetallesVenta;
    @FXML private TableColumn colCodigoDetalleVenta;
    @FXML private TableColumn colCantidad;
    @FXML private TableColumn colPrecioVenta;
    @FXML private TableColumn colProductoIva;
    @FXML private TableColumn colCodigoVenta;
    @FXML private TableColumn colCodigoProducto;
    
    @FXML private Button btnAgregar;
    @FXML private Button btnActualizar;
    @FXML private Button btnEliminar;
    @FXML private Button btnRegresar;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        desactivarControles();
        cargarComboBox();
    }
    
    public void cargarDatos() {
        tblDetallesVenta.setItems(getDetallesVenta(codigoVenta));
        colCodigoDetalleVenta.setCellValueFactory(new PropertyValueFactory<DetalleVentas, Integer>("codigoDetalleVenta"));
        colCantidad.setCellValueFactory(new PropertyValueFactory<DetalleVentas, Integer>("cantidad"));
        colPrecioVenta.setCellValueFactory(new PropertyValueFactory<DetalleVentas, Double>("precioVenta"));
        colProductoIva.setCellValueFactory(new PropertyValueFactory<DetalleVentas, Double>("productoIva"));
        colCodigoVenta.setCellValueFactory(new PropertyValueFactory<DetalleVentas, Integer>("codigoVenta"));
        colCodigoProducto.setCellValueFactory(new PropertyValueFactory<DetalleVentas, Integer>("codigoProducto"));
    }
    
    public void cargarComboBox() {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        getProductos();
        listaProductos.stream().filter((producto) -> (producto.getCodigoTipoProducto() == codigoTipoProducto)).forEachOrdered((producto) -> {
            lista.add(producto);
        });
        cmbCodigoProducto.setItems((FXCollections.observableArrayList(lista)));
        getVentas();
        getClientes();
    }
    
    public ObservableList<DetalleVentas> getDetallesVenta(int codigoVenta) {
        ArrayList<DetalleVentas> lista = new ArrayList<DetalleVentas>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarDetalleVentas(?)}");
            procedimiento.setInt(1, codigoVenta); //codigoVenta
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new DetalleVentas(resultado.getInt("codigoDetalleVenta"), resultado.getInt("cantidad"), resultado.getDouble("precioVenta"), resultado.getDouble("productoIVA"), resultado.getInt("codigoVenta"), resultado.getInt("codigoProducto")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaDetallesVenta = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<Productos> getProductos() {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarProductos}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Productos(resultado.getInt("codigoProducto"), resultado.getString("nombreProducto"), resultado.getInt("existencia"), resultado.getDouble("precioCosto"), resultado.getDouble("precioVenta"), resultado.getDouble("productoIva"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion"), resultado.getInt("codigoTipoProducto"), resultado.getInt("codigoProveedor")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaProductos = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<Ventas> getVentas() {
        ArrayList<Ventas> lista = new ArrayList<Ventas>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarVentas}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Ventas(resultado.getInt("codigoVenta"), resultado.getDate("fechaVenta"), resultado.getInt("iva"), resultado.getString("observacion"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion"), resultado.getDouble("total"), resultado.getInt("numeroFactura"), resultado.getInt("codigoCliente"), resultado.getInt("codigoFormaPago"), resultado.getInt("codigoTipoProducto"), resultado.getInt("codigoUsuario")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaVentas = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<Clientes> getClientes() {
        ArrayList<Clientes> lista = new ArrayList<Clientes>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarClientes}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Clientes(resultado.getInt("codigoCliente"), resultado.getString("nombreCliente"), resultado.getString("apellidoCliente"), resultado.getString("direccionCliente"), resultado.getString("nitCliente"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaClientes = FXCollections.observableArrayList(lista);
    }
    
    public void seleccionarElementos() {
        if(tblDetallesVenta.getSelectionModel().getSelectedIndex() > -1) {
            DetalleVentas elemento = (DetalleVentas)tblDetallesVenta.getSelectionModel().getSelectedItem();
            txtCantidad.setText(String.valueOf(elemento.getCantidad()));
            txtPrecioVenta.setText(String.valueOf(elemento.getPrecioVenta()));
            txtProductoIVA.setText(String.valueOf(elemento.getProductoIVA()));
            listaProductos.stream().filter((producto) -> (producto.getCodigoProducto() == elemento.getCodigoProducto())).forEachOrdered((producto) -> {
                cmbCodigoProducto.setValue(producto);
            });
            txtTotal.setText(String.valueOf(Double.parseDouble(txtPrecioVenta.getText()) * Integer.parseInt(txtCantidad.getText())));
        }
    }
    
    public void setTextFields() {
        try{
            if((cmbCodigoProducto.getSelectionModel().getSelectedItem())!= null){
                Double precio = null;
                Double iva = null;
                Double total = null;
                int cantidad;
                cantidad = Integer.parseInt(txtCantidad.getText());
                precio = (((Productos)cmbCodigoProducto.getSelectionModel().getSelectedItem()).getPrecioVenta());
                iva = (((Productos)cmbCodigoProducto.getSelectionModel().getSelectedItem()).getPrecioCosto()) * 0.12;
                total = (precio * cantidad);
                txtPrecioVenta.setText(String.valueOf(precio));
                txtProductoIVA.setText(String.valueOf(iva));
            }
        }catch(Exception e){
            txtCantidad.setText("0");
        }
    }
    
    public void setTotal() {
        if(!(txtCantidad.getText().equals(""))&&!(txtPrecioVenta.getText().equals(""))) {
            int cantidad;
            double precioVenta;
            try{
                cantidad = Integer.parseInt(txtCantidad.getText());
            }catch(Exception e) {
                cantidad = 0;
            }
            precioVenta = Double.parseDouble(txtPrecioVenta.getText());
            txtTotal.setText(String.valueOf(cantidad * precioVenta));
        }
    }
    
    public Ventas getVenta(int codigoVenta) {
        Ventas elemento = new Ventas();
        for(Ventas venta: listaVentas) {
            if(venta.getCodigoVenta() == codigoVenta) {
                elemento = venta;
            }
        }
        return elemento;
    }
    
    public Clientes getCliente(int codigoCliente) {
        Clientes elemento = new Clientes();
        for(Clientes cliente: listaClientes) {
            if(cliente.getCodigoCliente() == codigoCliente) {
                elemento = cliente;
            }
        }
        return elemento;
    }
    
    public void btnAgregar() {
        switch(operacionActual) {
            case NINGUNO:
                activarControles();
                limpiarControles();
                btnActualizar.setVisible(false);
                operacionActual = operaciones.AGREGAR;
            break;
            case AGREGAR:
                agregar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    btnActualizar.setVisible(true);
                    operacionActual = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnActualizar() {
        switch(operacionActual) {
            case NINGUNO:
                if(tblDetallesVenta.getSelectionModel().getSelectedItem() != null) {
                    String verificacion = JOptionPane.showInputDialog(null, "Para Actualizar el registro ingrese contraseña de administrador","Actualizar Detalle Venta",JOptionPane.INFORMATION_MESSAGE);
                    if(verificacion.equals("admin")) {
                        activarControles();
                        btnAgregar.setVisible(false);
                        operacionActual = operaciones.ACTUALIZAR;
                    }else {
                        JOptionPane.showMessageDialog(null, "Verifiación de Administrador no aprobada!");
                    }
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Detalle de Venta");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    btnAgregar.setVisible(true);
                    operacionActual = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnEliminar() {
        switch(operacionActual) {
            case NINGUNO:
                if(tblDetallesVenta.getSelectionModel().getSelectedItem() != null) {
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Detalle Venta",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION) {
                        String verificacion = JOptionPane.showInputDialog(null, "Para Actualizar el registro ingrese contraseña de administrador","Actualizar Detalle Venta",JOptionPane.INFORMATION_MESSAGE);
                        if(verificacion.equals("admin")) {
                            eliminar();
                            limpiarControles();
                            cargarDatos();
                        }else {
                            JOptionPane.showMessageDialog(null, "Verifiación de Administrador no aprobada!");
                        }
                    }
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Detalle de Venta");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void agregar() {
        boolean entrar = true;
        int cantidad = 0;
        try{
            cantidad = Integer.parseInt((txtCantidad.getText()));
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "El campo 'Cantidad' debe de ser un número");
            entrar = false;
            camposCorrectos = false;
        }
        try{
            if(!(txtCantidad.getText().equals(""))&&!(txtPrecioVenta.getText().equals(""))&&!(txtProductoIVA.getText().equals(""))&&!(cmbCodigoProducto.getValue().equals(""))&&(entrar)) {
                do {
                    PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_InsertarDetalleVentas(?,?,?,?,?,?,?,?,?,?)}");
                    procedimiento.setInt(1, (Integer.parseInt(txtCantidad.getText()))); //cantidad
                    procedimiento.setDouble(2, (Double.parseDouble(txtPrecioVenta.getText()))); //precioVenta
                    procedimiento.setDouble(3, (Double.parseDouble(txtProductoIVA.getText()))); //productoIva
                    procedimiento.setInt(4, (codigoVenta)); //codigoVenta
                    procedimiento.setInt(5, (((Productos)cmbCodigoProducto.getSelectionModel().getSelectedItem()).getCodigoProducto())); //codigoProducto
                    procedimiento.setInt(6, (getVenta(codigoVenta).getNumeroFactura())); //numeroFactura
                    procedimiento.setDate(7, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(getVenta(codigoVenta).getFechaVenta())))); //fecha
                    procedimiento.setString(8, (getCliente(getVenta(codigoVenta).getCodigoCliente()).getNitCliente())); //nitCliente
                    procedimiento.setDouble(9, (Double.parseDouble(txtTotal.getText()))); //total
                    procedimiento.setInt(10, (getVenta(codigoVenta).getCodigoCliente())); //codigoCliente
                    if(cantidad <= 0) {
                        JOptionPane.showMessageDialog(null, "La cantidad no puede ser 0 o negativa!");
                    }
                    if(cantidad > 0) {
                        Productos producto = (Productos)cmbCodigoProducto.getSelectionModel().getSelectedItem();
                        if((cantidad) > (producto.getExistencia())) {
                            JOptionPane.showMessageDialog(null, "La cantidad es mayor a la existencia del producto!");
                            cantidad = 0;
                        }else {
                            procedimiento.execute();
                            camposCorrectos = true;
                            cantidad = 0;
                        }
                    }
                }while(cantidad > 0);
            }else {
                if(entrar) {
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    camposCorrectos = false;
                }
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void actualizar() {
        boolean entrar = true;
        int cantidad = 0;
        try{
            cantidad = Integer.parseInt((txtCantidad.getText()));
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "El campo 'Cantidad' debe de ser un número");
            entrar = false;
            camposCorrectos = false;
        }
        try{
            if(!(txtCantidad.getText().equals(""))&&!(txtPrecioVenta.getText().equals(""))&&!(txtProductoIVA.getText().equals(""))&&!(cmbCodigoProducto.getValue().equals(""))&&(entrar)) {
                do{
                    PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ModificarDetalleVentas(?,?,?,?,?,?,?,?,?,?,?,?)}");
                    procedimiento.setInt(1, (Integer.parseInt(txtCantidad.getText()))); //cantidad
                    procedimiento.setDouble(2, (Double.parseDouble(txtPrecioVenta.getText()))); //precioVenta
                    procedimiento.setDouble(3, (Double.parseDouble(txtProductoIVA.getText()))); //productoIva
                    procedimiento.setInt(4, (codigoVenta)); //codigoVenta
                    procedimiento.setInt(5, (((Productos)cmbCodigoProducto.getSelectionModel().getSelectedItem()).getCodigoProducto())); //codigoProducto
                    procedimiento.setInt(6, (((DetalleVentas)tblDetallesVenta.getSelectionModel().getSelectedItem()).getCodigoDetalleVenta())); //codigoDetalleVenta
                    procedimiento.setInt(7, (getVenta(codigoVenta).getNumeroFactura())); //numeroFactura
                    procedimiento.setDate(8, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(getVenta(codigoVenta).getFechaVenta())))); //fecha
                    procedimiento.setString(9, (getCliente(getVenta(codigoVenta).getCodigoCliente()).getNitCliente())); //nitCliente
                    procedimiento.setDouble(10, (Double.parseDouble(txtTotal.getText()))); //total
                    procedimiento.setInt(11, (getVenta(codigoVenta).getCodigoCliente())); //codigoCliente
                    try{
                        int codigoFactura = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese un correcto CÓDIGO DE FACTURA","Actualizar Detalle de Venta",JOptionPane.INFORMATION_MESSAGE));
                        procedimiento.setInt(12, (codigoFactura)); //codigoFactura
                        if(cantidad <= 0) {
                            JOptionPane.showMessageDialog(null, "La cantidad no puede ser 0 o negativa!");
                        }
                        if(cantidad > 0) {
                            Productos producto = (Productos)cmbCodigoProducto.getSelectionModel().getSelectedItem();
                            if((cantidad) > (producto.getExistencia())) {
                                JOptionPane.showMessageDialog(null, "La cantidad es mayor a la existencia del producto!");
                                cantidad = 0;
                            }else {
                                procedimiento.execute();
                                camposCorrectos = true;
                                cantidad = 0;
                            }
                        }
                    }catch(SQLException e) {
                        JOptionPane.showMessageDialog(null, "el Código de Factura ingresado no es válido");
                        cantidad = 0;
                    }catch(Exception e) {
                        JOptionPane.showMessageDialog(null, "el Código de Factura ingresado no es válido");
                        cantidad = 0;
                    }
                }while(cantidad > 0);
            }else {
                if(entrar) {
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    camposCorrectos = false;
                }
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void eliminar() {
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_EliminarDetalleVentas(?,?)}");
            procedimiento.setInt(1, (((DetalleVentas)tblDetallesVenta.getSelectionModel().getSelectedItem()).getCodigoDetalleVenta())); //codigoDetalleVenta
            try{
                int codigoFactura = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese un correcto CÓDIGO DE FACTURA","Eliminar Detalle de Venta",JOptionPane.INFORMATION_MESSAGE));
                procedimiento.setInt(12, (codigoFactura)); //codigoFactura
                procedimiento.execute();
            }catch(SQLException e) {
                JOptionPane.showMessageDialog(null, "el Código de Factura ingresado no es válido");
            }catch(Exception e) {
                JOptionPane.showMessageDialog(null, "el Código de Factura ingresado no es válido");
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void desactivarControles() {
        txtCantidad.setDisable(true);
        txtPrecioVenta.setDisable(true);
        txtProductoIVA.setDisable(true);
        cmbCodigoProducto.setDisable(true);
        txtTotal.setDisable(true);
    }
    
    public void activarControles() {
        txtCantidad.setDisable(false);
        cmbCodigoProducto.setDisable(false);
        txtTotal.setDisable(false);
        txtTotal.setEditable(false);
    }
    
    public void limpiarControles() {
        txtCantidad.setText("");
        txtPrecioVenta.setText("");
        txtProductoIVA.setText("");
        cmbCodigoProducto.setValue("");
        txtTotal.setText("");
    }
    
    public void cancelar() {
        tblDetallesVenta.getSelectionModel().clearSelection();
        desactivarControles();
        limpiarControles();
        cargarDatos();
        btnAgregar.setVisible(true);
        btnActualizar.setVisible(true);
        operacionActual = operaciones.NINGUNO;
    }
    
    public void escenaMenu() {
        this.escenario.escenaMenu();
    }

    public Principal getEscenario() {
        return escenario;
    }

    public void setEscenario(Principal escenario) {
        this.escenario = escenario;
    }

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
        cargarDatos();
    }

    public int getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    public void setCodigoTipoProducto(int codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
        cargarComboBox();
    }
    
}

