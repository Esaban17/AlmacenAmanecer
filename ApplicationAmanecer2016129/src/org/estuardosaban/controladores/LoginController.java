package org.estuardosaban.controladores;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javax.swing.JOptionPane;
import org.estuardosaban.modelos.Usuarios;
import org.estuardosaban.db.Conexion;
import org.estuardosaban.sistema.Principal;

public class LoginController implements Initializable {
    
    public static int codigoUsuario;
    public static String nombre;
    public static String apellido;
    
    private Principal escenario;
    @FXML private TextField txtUsuario;
    @FXML private PasswordField pssContrasena;
    @FXML private CheckBox chkMostrar;
    @FXML private Button btnLogin;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        txtUsuario.setText("");
        pssContrasena.setText("");
    }
    
    public Usuarios getUsuario() {
        Usuarios usuario = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarUsuarios(?)}");
            procedimiento.setString(1, (txtUsuario.getText()));
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                usuario = new Usuarios(resultado.getInt("codigoUsuario"), resultado.getString("nombreUsuario"), resultado.getString("apellidoUsuario"), resultado.getString("login"), resultado.getString("contrasena"));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return usuario;
    }
    
    public void validacion() {
        String login;
        String contrasena;
        try{
            codigoUsuario = getUsuario().getCodigoUsuario();
            nombre = getUsuario().getNombreUsuario();
            apellido = getUsuario().getApellidoUsuario();
            login = getUsuario().getLogin();
            contrasena = getUsuario().getContrasena();
            if((login.equals(txtUsuario.getText())) && (contrasena.equals(pssContrasena.getText()))){
                
                escenaMenu();
            }else{
                JOptionPane.showMessageDialog(null, "¡Usuario y constraseña no coinciden!");
            }
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "¡El usuario no existe!");
        }
    }
    
    public void mostrarContrasena() {
        if(chkMostrar.isSelected()){
            pssContrasena.isVisible();
            chkMostrar.setText("Ocultar Contraseña");
        }else{
            chkMostrar.setText("Mostrar Contraseña");
        }
    }

    public void escenaMenu() {
        this.escenario.escenaMenu();
    }
    
    public Principal getEscenario() {
        return escenario;
    }

    public void setEscenario(Principal escenario) {
        this.escenario = escenario;
    }
    
}
