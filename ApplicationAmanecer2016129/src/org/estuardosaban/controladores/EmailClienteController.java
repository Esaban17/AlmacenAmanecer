package org.estuardosaban.controladores;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.JOptionPane;

import org.estuardosaban.modelos.EmailCliente;
import org.estuardosaban.modelos.Clientes;
import org.estuardosaban.sistema.Principal;
import org.estuardosaban.db.Conexion;

public class EmailClienteController implements Initializable {
    
    private enum operaciones{NUEVO, GUARDAR, ELIMINAR, ACTUALIZAR, NINGUNO}
    private Principal escenario;
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<EmailCliente>listaEmailCliente; 
    private ObservableList<Clientes>listaClientes;   
    @FXML private TextField txtEmailCliente;
    @FXML private TextField txtDescripcion;
    @FXML private ComboBox cmbCliente;
    @FXML private TableView tblEmailCliente;
    @FXML private TableColumn colCodigo;
    @FXML private TableColumn colEmailCliente;
    @FXML private TableColumn colDescripcion;
    @FXML private TableColumn colCliente;
    @FXML private Button btnNuevo;
    @FXML private Button btnEditar;
    @FXML private Button btnEliminar;
    @FXML private Button btnRegresar;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cmbCliente.setItems(getClientes());
        desactivarControles();
        cargarDatos();        
    }
    
    public Clientes buscarCliente(int codCliente){
        Clientes resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarClientes(?)}");
            procedimiento.setInt(1, codCliente);
            ResultSet registro= procedimiento.executeQuery();
            while (registro.next()){
                resultado = new Clientes (registro.getInt("codigoCliente"),registro.getString("nombreCliente"),registro.getString("apellidoCliente"),registro.getString("direccionCliente"),registro.getString("nitCliente"),registro.getDate("fechaCreacion"),registro.getDate("fechaModificacion"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }

    public EmailCliente buscarEmailCliente(int codEmailCliente){
        EmailCliente resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarEmail_Cliente(?)}");
            procedimiento.setInt(1, codEmailCliente);
            ResultSet registro = procedimiento.executeQuery();
            while(registro.next()){
                resultado = new EmailCliente(registro.getInt("codigoEmailCliente"),registro.getString("email"),registro.getString("descripcion"),registro.getInt("codigoCliente"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    public ObservableList<Clientes> getClientes(){
        ArrayList<Clientes> lista = new ArrayList<Clientes>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarClientes}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new Clientes (resultado.getInt("codigoCliente"),resultado.getString("nombreCliente"),resultado.getString("apellidoCliente"),resultado.getString("direccionCliente"),resultado.getString("nitCliente"),resultado.getDate("fechaCreacion"),resultado.getDate("fechaModificacion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaClientes = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<EmailCliente> getEmailCliente(){
        ArrayList<EmailCliente> lista = new ArrayList<EmailCliente>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarEmail_Cliente}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new EmailCliente (resultado.getInt("codigoEmailCliente"),resultado.getString("email"),resultado.getString("descripcion"),resultado.getInt("codigoCliente")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaEmailCliente = FXCollections.observableArrayList(lista);
    }
    
    boolean ward = true;
    public void nuevo(){
        switch(tipoOperaciones){
            case NINGUNO:
                activarControles();
                limpiarControles();
                btnNuevo.setText("GUARDAR");
                btnEliminar.setText("CANCELAR");
                btnEditar.setDisable(true);
                tipoOperaciones=operaciones.GUARDAR;
            break;
            case GUARDAR:
                agregar();
                if(ward == true){
                    btnNuevo.setText("NUEVO");
                    btnEliminar.setText("ELIMINAR");
                    btnEditar.setDisable(false);
                    tipoOperaciones=operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    }
    public void editar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblEmailCliente.getSelectionModel().getSelectedItem() != null){
                    btnEditar.setText("ACTUALIZAR");
                    btnEliminar.setText("CANCELAR");
                    btnNuevo.setDisable(true);
                    tipoOperaciones = operaciones.ACTUALIZAR;
                    txtEmailCliente.setEditable(true);
                    txtDescripcion.setEditable(true);
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Email");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(ward == true){
                    btnEditar.setText("EDITAR");
                    btnEliminar.setText("ELIMINAR");
                    btnNuevo.setDisable(false);
                    txtEmailCliente.setEditable(false);
                    txtDescripcion.setEditable(false);
                    tipoOperaciones = operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    }
    
    public void eliminar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblEmailCliente.getSelectionModel().getSelectedItem() != null){
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Email Cliente",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION){
                        borrar();
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Email");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void cancelar(){
        escenario.escenaClientes();
    }
    
    public void agregar(){
        try{
            if(!(txtEmailCliente.getText().equals(""))&&!(txtDescripcion.getText().equals(""))&&!cmbCliente.getValue().equals("")){
                EmailCliente registro = new EmailCliente();
                registro.setEmail(txtEmailCliente.getText());
                registro.setDescripcion(txtDescripcion.getText());
                registro.setCodigoCliente(((Clientes)cmbCliente.getSelectionModel().getSelectedItem()).getCodigoCliente());
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_InsertarEmail_Cliente(?,?,?)}");
                procedimiento.setString(1, registro.getEmail());
                procedimiento.setString(2, registro.getDescripcion());
                procedimiento.setInt(3, registro.getCodigoCliente());
                procedimiento.execute();
                listaEmailCliente.add(registro);
                limpiarControles();
                ward = true;
            }else{
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                ward = false;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    public void actualizar(){
        try{
            if(!(txtEmailCliente.getText().equals(""))&&!(txtDescripcion.getText().equals(""))&&!(cmbCliente.getValue().equals(""))){
                EmailCliente registro = (EmailCliente) tblEmailCliente.getSelectionModel().getSelectedItem();
                registro.setEmail(txtEmailCliente.getText());
                registro.setDescripcion(txtDescripcion.getText());
                registro.setCodigoCliente(((Clientes)cmbCliente.getSelectionModel().getSelectedItem()).getCodigoCliente());
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ModificarEmail_Cliente(?,?,?,?)}");
                procedimiento.setInt(1,registro.getCodigoEmailCliente());
                procedimiento.setString(2, registro.getEmail());
                procedimiento.setString(3, registro.getDescripcion());
                procedimiento.setInt(4, registro.getCodigoCliente());
                procedimiento.execute();
                listaEmailCliente.add(registro);
                ward = true;
                limpiarControles();
            }else{
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                ward = false;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void borrar(){
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_EliminarEmail_Cliente(?)}");
            procedimiento.setInt(1, ((EmailCliente)tblEmailCliente.getSelectionModel().getSelectedItem()).getCodigoEmailCliente());
            procedimiento.execute();
            listaEmailCliente.remove(tblEmailCliente.getSelectionModel().getSelectedIndex());
            limpiarControles();
            cargarDatos();
            tipoOperaciones = operaciones.NINGUNO;
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void seleccionarElementos(){
        if(tblEmailCliente.getSelectionModel().getSelectedIndex()>-1){
            txtEmailCliente.setText(((EmailCliente)tblEmailCliente.getSelectionModel().getSelectedItem()).getEmail());
            txtDescripcion.setText(String.valueOf(((EmailCliente)tblEmailCliente.getSelectionModel().getSelectedItem()).getDescripcion()));                     
            cmbCliente.getSelectionModel().select(buscarCliente(((EmailCliente)tblEmailCliente.getSelectionModel().getSelectedItem()).getCodigoCliente()));
        }
    }

    public void cargarDatos(){
        desactivarControles();
        tblEmailCliente.setItems(getEmailCliente());
        colCodigo.setCellValueFactory(new PropertyValueFactory<EmailCliente, Integer>("codigoEmailCliente"));
        colEmailCliente.setCellValueFactory(new PropertyValueFactory<EmailCliente, String>("email"));
        colDescripcion.setCellValueFactory(new PropertyValueFactory<EmailCliente, String>("descripcion"));
        colCliente.setCellValueFactory(new PropertyValueFactory<EmailCliente, Integer>("codigoCliente"));
    }
    
    public void activarControles(){
        txtEmailCliente.setEditable(true);
        txtDescripcion.setEditable(true);
        cmbCliente.setDisable(false);
    }
    
    public void desactivarControles(){
        txtEmailCliente.setEditable(false);
        txtDescripcion.setEditable(false);
        cmbCliente.setDisable(true);
    }
    
    public void limpiarControles(){
        txtEmailCliente.setText("");
        txtDescripcion.setText("");
        cmbCliente.setValue("");
    }
    
    public Principal getEscenario() {
        return escenario;
    }

    public void setEscenario(Principal escenario) {
        this.escenario = escenario;
    }
    
    public void escenaClientes(){
        escenario.escenaClientes();
    }
}
