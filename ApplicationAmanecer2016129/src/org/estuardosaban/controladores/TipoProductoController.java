package org.estuardosaban.controladores;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import org.estuardosaban.sistema.Principal;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.JOptionPane;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import javafx.scene.image.ImageView;
import org.estuardosaban.modelos.Clientes;
import org.estuardosaban.db.Conexion;
import org.estuardosaban.modelos.TipoProducto;

public class TipoProductoController implements Initializable{
    
    private enum operaciones{NUEVO, GUARDAR, ELIMINAR, ACTUALIZAR, NINGUNO}
    private Principal escenario;
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<TipoProducto>listaTipoProducto;    
    @FXML private TextField txtDescripcion;
    @FXML private TableView tblTipoProducto;
    @FXML private TableColumn colCodigo;
    @FXML private TableColumn colDescripcion;
    @FXML private TableColumn colFechaCreacion;
    @FXML private TableColumn colFechaModificacion;
    @FXML private Button btnNuevo;
    @FXML private Button btnEditar;
    @FXML private Button btnEliminar;
    @FXML private Button btnRegresar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
    }
    
    public void cargarDatos(){
        tblTipoProducto.setItems(getTipoProducto());
        colCodigo.setCellValueFactory(new PropertyValueFactory<Clientes, Integer>("codigoTipoProducto"));
        colDescripcion.setCellValueFactory(new PropertyValueFactory<Clientes, String>("descripcion"));
        colFechaCreacion.setCellValueFactory(new PropertyValueFactory<Clientes, Date>("fechaCreacion"));
        colFechaModificacion.setCellValueFactory(new PropertyValueFactory<Clientes, Date>("fechaModificacion"));
        txtDescripcion.setEditable(false);
    }
    
    public ObservableList<TipoProducto> getTipoProducto(){
        ArrayList<TipoProducto> lista = new ArrayList<TipoProducto>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarTipoProducto}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new TipoProducto (resultado.getInt("codigoTipoProducto"),resultado.getString("descripcion"),resultado.getDate("fechaCreacion"),resultado.getDate("fechaModificacion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaTipoProducto = FXCollections.observableArrayList(lista);
    }   
    
    public void seleccionarElementos(){
        if(tblTipoProducto.getSelectionModel().getSelectedIndex()>-1){
            txtDescripcion.setText(((TipoProducto)tblTipoProducto.getSelectionModel().getSelectedItem()).getDescripcion());
        }
    }
    
    public TipoProducto buscarTipoProducto(int codTipoProducto){
        TipoProducto resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarTipoProducto(?)}");
            procedimiento.setInt(1, codTipoProducto);
            ResultSet registro = procedimiento.executeQuery();
            while(registro.next()){
                resultado = new TipoProducto(registro.getInt("codigoTipoProducto"),registro.getString("descripcion"),registro.getDate("fechaCreacion"),registro.getDate("fechaModificacion"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    boolean ward = true;
    public void nuevo(){
        switch(tipoOperaciones){
            case NINGUNO:
                activarControles();
                limpiarControles();
                btnNuevo.setText("GUARDAR");
                btnEliminar.setText("CANCELAR");
                btnEditar.setDisable(true);
                tipoOperaciones=TipoProductoController.operaciones.GUARDAR;
            break;
            case GUARDAR:
                agregar();
                if(ward == true){
                    btnNuevo.setText("NUEVO");
                    btnEliminar.setText("ELIMINAR");
                    btnEditar.setDisable(false);
                    tipoOperaciones=TipoProductoController.operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    } 
    
    public void editar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblTipoProducto.getSelectionModel().getSelectedItem() != null){
                    btnEditar.setText("ACTUALIZAR");
                    btnEliminar.setText("CANCELAR");
                    btnNuevo.setDisable(true);
                    tipoOperaciones = TipoProductoController.operaciones.ACTUALIZAR;
                    txtDescripcion.setEditable(true);
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Tipo de Producto");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(ward == true){
                    btnEditar.setText("EDITAR");
                    btnEliminar.setText("ELIMINAR");
                    btnNuevo.setDisable(false);
                    txtDescripcion.setEditable(false);
                    tipoOperaciones = TipoProductoController.operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    }

    public void eliminar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblTipoProducto.getSelectionModel().getSelectedItem() != null){
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Tipo de Producto",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION){
                        borrar();
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Tipo de Producto");
                }
            break;
            default:
                cancelar();
            break;
        }
    }

    public void agregar(){
        boolean entrar = true;
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String fechaMod = "Sin Modificar";
        try{
            if(!(txtDescripcion.getText().equals(""))&&(entrar==true)){
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_InsertarTipoProducto(?,?)}");
                TipoProducto registro = new TipoProducto();
                registro.setDescripcion(txtDescripcion.getText());
                procedimiento.setString(1, registro.getDescripcion());
                procedimiento.setString(2, dateFormat.format(date));
                procedimiento.execute();
                listaTipoProducto.add(registro);
                cargarDatos();
                ward = true;
            }else{
                if(entrar == true){
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    ward = false;
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

        public void actualizar(){
           DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
           Date date = new Date();
        try{
            if(!(txtDescripcion.getText().equals(""))){
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ModificarTipoProducto(?,?,?)}");
                TipoProducto registro = (TipoProducto)tblTipoProducto.getSelectionModel().getSelectedItem();
                registro.setDescripcion(txtDescripcion.getText());
                procedimiento.setInt(1, registro.getCodigoTipoProducto());
                procedimiento.setString(2, registro.getDescripcion());
                procedimiento.setString(3, dateFormat.format(date));
                procedimiento.execute();
                ward = true;
            }else{
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                ward = false;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
     
    public void borrar(){
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_EliminarTipoProducto(?)}");
            procedimiento.setInt(1, ((TipoProducto)tblTipoProducto.getSelectionModel().getSelectedItem()).getCodigoTipoProducto());
            procedimiento.execute();
            listaTipoProducto.remove(tblTipoProducto.getSelectionModel().getSelectedIndex());
            limpiarControles();
            cargarDatos();
            tipoOperaciones = operaciones.NINGUNO;
        }catch(SQLException e){
            e.printStackTrace();
        }
    } 
    
    public void limpiarControles(){
        txtDescripcion.setText("");
    }
    
    public void activarControles(){
        txtDescripcion.setEditable(true);
    }
    
    public void desactivarControles(){
        txtDescripcion.setEditable(false);
    }
    
    
    public void cancelar(){
        escenario.escenaTipoProducto();
    }

    public Principal getEscenario() {
        return escenario;
    }

    public void setEscenario(Principal escenario) {
        this.escenario = escenario;
    }
    
    public void escenaMenu(){
        this.escenario.escenaMenu();
    }
}
