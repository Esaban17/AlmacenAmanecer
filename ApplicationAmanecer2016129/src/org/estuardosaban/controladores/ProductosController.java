package org.estuardosaban.controladores;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.JOptionPane;

import org.estuardosaban.modelos.TipoProducto;
import org.estuardosaban.modelos.Proveedores;
import org.estuardosaban.sistema.Principal;
import org.estuardosaban.modelos.Productos;
import org.estuardosaban.db.Conexion;

public class ProductosController implements Initializable{
    
    private Principal escenario;
    private enum operaciones{NUEVO, GUARDAR, ELIMINAR, ACTUALIZAR, NINGUNO}
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<Productos> listaProductos;
    private ObservableList<TipoProducto> listaTipoProducto;
    private ObservableList<Proveedores> listaProveedores;  
    
    @FXML private TextField txtNombreProducto;
    @FXML private TextField txtExistencia;
    @FXML private TextField txtPrecioCosto;
    @FXML private TextField txtPrecioVenta;
    @FXML private TextField txtProductoIVA;
    @FXML private ComboBox cmbTipoProducto;
    @FXML private ComboBox cmbProveedor;
    
    
    @FXML private TableView tblProductos;
    @FXML private TableColumn colCodigo;
    @FXML private TableColumn colNombreProducto;
    @FXML private TableColumn colExistencia;
    @FXML private TableColumn colPrecioCosto;
    @FXML private TableColumn colPrecioVenta;
    @FXML private TableColumn colProductoIVA;
    @FXML private TableColumn colFechaCreacion;
    @FXML private TableColumn colFechaModificacion;
    @FXML private TableColumn colTipoProducto;
    @FXML private TableColumn colProveedor;
    
    
    @FXML private Button btnNuevo;
    @FXML private Button btnEditar;
    @FXML private Button btnEliminar;
    @FXML private Button btnReporte;
    @FXML private Button btnRegresar;
    @FXML private Button btnTelefono;
    @FXML private Button btnEmail;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cmbProveedor.setItems(getProveedores());
        cmbTipoProducto.setItems(getTipoProducto());
        desactivarControles();
        cargarDatos();
    }
    
    public Productos buscarProducto(int codigoProducto){
        Productos resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarProductos(?)}");
            procedimiento.setInt(1, codigoProducto);
            ResultSet registro= procedimiento.executeQuery();
            while (registro.next()){
                resultado = new Productos (registro.getInt("codigoProducto"), registro.getString("nombreProducto"),registro.getInt("existencia"),registro.getDouble("precioCosto"),registro.getDouble("precioVenta"),registro.getDouble("productoIva"),registro.getDate("fechaCreacion"),registro.getDate("fechaModificacion"),registro.getInt("codigoTipoProducto"),registro.getInt("codigoProveedor"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    public Proveedores buscarProveedor(int codProveedor){
        Proveedores resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarProveedores(?)}");
            procedimiento.setInt(1, codProveedor);
            ResultSet registro = procedimiento.executeQuery();
            while(registro.next()){
                resultado = new Proveedores(registro.getInt("codigoProveedor"), registro.getString("razonSocial"),registro.getString("nit"),registro.getString("direccionProveedor"),registro.getString("paginaWeb"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    public TipoProducto buscarTipoProducto(int codTipoProducto){
        TipoProducto resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarTipoProducto(?)}");
            procedimiento.setInt(1, codTipoProducto);
            ResultSet registro = procedimiento.executeQuery();
            while(registro.next()){
                resultado = new TipoProducto(registro.getInt("codigoTipoProducto"), registro.getString("descripcion"),registro.getDate("fechaCreacion"),registro.getDate("fechaModificacion"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }  
    
    public ObservableList<Productos> getProductos(){
        ArrayList<Productos> lista = new ArrayList<Productos>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarProductos}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new Productos (resultado.getInt("codigoProducto"),resultado.getString("nombreProducto"),resultado.getInt("existencia"),resultado.getDouble("precioCosto"),resultado.getDouble("precioVenta"),resultado.getDouble("productoIVA"),resultado.getDate("fechaCreacion"),resultado.getDate("fechaModificacion"),resultado.getInt("codigoTipoProducto"),resultado.getInt("codigoProveedor")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaProductos = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<TipoProducto> getTipoProducto(){
        ArrayList<TipoProducto> lista = new ArrayList<TipoProducto>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarTipoProducto}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new TipoProducto (resultado.getInt("codigoTipoProducto"),resultado.getString("descripcion"),resultado.getDate("fechaCreacion"),resultado.getDate("fechaModificacion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaTipoProducto = FXCollections.observableArrayList(lista);
    }

    public ObservableList<Proveedores> getProveedores(){
        ArrayList<Proveedores> lista = new ArrayList<Proveedores>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarProveedores}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new Proveedores (resultado.getInt("codigoProveedor"),resultado.getString("razonSocial"),resultado.getString("nit"),resultado.getString("direccionProveedor"),resultado.getString("paginaWeb")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaProveedores = FXCollections.observableArrayList(lista);
    }
    
    boolean ward = true;
    public void nuevo(){
        switch(tipoOperaciones){
            case NINGUNO:
                activarControles();
                limpiarControles();
                btnNuevo.setText("GUARDAR");
                btnEliminar.setText("CANCELAR");
                btnEditar.setDisable(true);
                btnReporte.setDisable(true);
                tipoOperaciones=operaciones.GUARDAR;
            break;
            case GUARDAR:
                agregar();
                if(ward == true){
                    btnNuevo.setText("NUEVO");
                    btnEliminar.setText("ELIMINAR");
                    btnEditar.setDisable(false);
                    btnReporte.setDisable(false);
                    tipoOperaciones=operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    }
    
    public void editar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblProductos.getSelectionModel().getSelectedItem() != null){
                    btnEditar.setText("ACTUALIZAR");
                    btnEliminar.setText("CANCELAR");
                    btnNuevo.setDisable(true);
                    btnReporte.setDisable(true);
                    tipoOperaciones = operaciones.ACTUALIZAR;
                    activarControles();
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Producto");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(ward == true){
                    btnEditar.setText("EDITAR");
                    btnEliminar.setText("ELIMINAR");
                    btnNuevo.setDisable(false);
                    btnReporte.setDisable(false);
                    txtNombreProducto.setEditable(false);
                    txtExistencia.setEditable(false);
                    txtPrecioCosto.setEditable(false);
                    tipoOperaciones = operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    }

    public void agregar(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        txtPrecioVenta.setVisible(true);
        txtProductoIVA.setVisible(true);
        try{
            if(!(txtNombreProducto.getText().equals(""))&&!(cmbTipoProducto.getValue().equals(""))&&!(cmbProveedor.getValue().equals(""))){
                Productos registro = new Productos();
                registro.setNombreProducto(txtNombreProducto.getText());
                registro.setExistencia(Integer.parseInt(txtExistencia.getText()));
                registro.setPrecioCosto(Double.parseDouble(txtPrecioCosto.getText()));
                registro.setCodigoTipoProducto(((TipoProducto)cmbTipoProducto.getSelectionModel().getSelectedItem()).getCodigoTipoProducto());
                registro.setCodigoProveedor(((Proveedores)cmbProveedor.getSelectionModel().getSelectedItem()).getCodigoProveedor());
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_InsertarProductos(?,?,?,?,?,?,?,?)}");
                procedimiento.setString(1, registro.getNombreProducto());
                procedimiento.setInt(2, 0);
                procedimiento.setDouble(3, 0.00);
                procedimiento.setDouble(4, 0.00);
                procedimiento.setDouble(5, 0.00);
                procedimiento.setString(6, dateFormat.format(date));
                procedimiento.setInt(7, registro.getCodigoTipoProducto());
                procedimiento.setInt(8, registro.getCodigoProveedor());
                procedimiento.execute();
                listaProductos.add(registro);
                limpiarControles();
                ward = true;
            }else{
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                ward = false;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void actualizar(){
        Double precioVenta;
        Double productoIVA;
        try{
            if(!(txtNombreProducto.getText().equals(""))&&!(cmbTipoProducto.getValue().equals(""))&&!(cmbProveedor.getValue().equals(""))){
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                Productos registro = (Productos) tblProductos.getSelectionModel().getSelectedItem();
                registro.setNombreProducto(txtNombreProducto.getText());
                registro.setExistencia(Integer.parseInt(txtExistencia.getText()));
                registro.setPrecioCosto(Double.parseDouble(txtPrecioCosto.getText()));
                registro.setPrecioVenta(Double.parseDouble(txtPrecioVenta.getText()));
                registro.setProductoIVA(Double.parseDouble(txtProductoIVA.getText()));
                registro.setCodigoTipoProducto(((TipoProducto)cmbTipoProducto.getSelectionModel().getSelectedItem()).getCodigoTipoProducto());
                registro.setCodigoProveedor(((Proveedores)cmbProveedor.getSelectionModel().getSelectedItem()).getCodigoProveedor());
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ModificarProductos(?,?,?,?,?,?,?,?,?)}");
                procedimiento.setInt(1,registro.getCodigoProducto());
                procedimiento.setString(2, registro.getNombreProducto());
                procedimiento.setInt(3, registro.getExistencia());
                procedimiento.setDouble(4, registro.getPrecioCosto());
                procedimiento.setDouble(5, registro.getPrecioVenta());
                procedimiento.setDouble(6, registro.getProductoIVA());
                procedimiento.setString(7, dateFormat.format(date));
                procedimiento.setInt(8, registro.getCodigoTipoProducto());
                procedimiento.setInt(9, registro.getCodigoProveedor());
                procedimiento.execute();
                listaProductos.add(registro);
                ward = true;
                limpiarControles();
            }else{
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                ward = false;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void eliminar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblProductos.getSelectionModel().getSelectedItem() != null){
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Producto",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION){
                        borrar();
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Producto");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void borrar(){
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_EliminarProductos(?)}");
            procedimiento.setInt(1, ((Productos)tblProductos.getSelectionModel().getSelectedItem()).getCodigoProducto());
            procedimiento.execute();
            listaProductos.remove(tblProductos.getSelectionModel().getSelectedIndex());
            limpiarControles();
            cargarDatos();
            tipoOperaciones = operaciones.NINGUNO;
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void cancelar(){
        escenario.escenaProductos();
    }
    
    public void seleccionarElementos(){
        txtPrecioVenta.setVisible(true);
        txtProductoIVA.setVisible(true);
        if(tblProductos.getSelectionModel().getSelectedIndex()>-1){
            txtNombreProducto.setText(((Productos)tblProductos.getSelectionModel().getSelectedItem()).getNombreProducto());
            txtExistencia.setText(String.valueOf(((Productos)tblProductos.getSelectionModel().getSelectedItem()).getExistencia()));
            txtPrecioCosto.setText(String.valueOf(((Productos)tblProductos.getSelectionModel().getSelectedItem()).getPrecioCosto()));
            txtPrecioVenta.setText(String.valueOf(((Productos)tblProductos.getSelectionModel().getSelectedItem()).getPrecioVenta()));            
            txtProductoIVA.setText(String.valueOf(((Productos)tblProductos.getSelectionModel().getSelectedItem()).getProductoIVA()));           
            cmbTipoProducto.getSelectionModel().select(buscarTipoProducto(((Productos)tblProductos.getSelectionModel().getSelectedItem()).getCodigoTipoProducto()));
            cmbProveedor.getSelectionModel().select(buscarProveedor(((Productos)tblProductos.getSelectionModel().getSelectedItem()).getCodigoProveedor()));
        }
    }
    
        
    public void cargarDatos(){
        desactivarControles();
        tblProductos.setItems(getProductos());
        colCodigo.setCellValueFactory(new PropertyValueFactory<Productos, Integer>("codigoProducto"));
        colNombreProducto.setCellValueFactory(new PropertyValueFactory<Productos, String>("nombreProducto"));
        colExistencia.setCellValueFactory(new PropertyValueFactory<Productos, Integer>("existencia"));
        colPrecioCosto.setCellValueFactory(new PropertyValueFactory<Productos, Double>("precioCosto"));
        colPrecioVenta.setCellValueFactory(new PropertyValueFactory<Productos, Double>("precioVenta"));
        colProductoIVA.setCellValueFactory(new PropertyValueFactory<Productos, Double>("productoIVA"));
        colFechaCreacion.setCellValueFactory(new PropertyValueFactory<Productos, Date>("fechaCreacion"));
        colFechaModificacion.setCellValueFactory(new PropertyValueFactory<Productos, Date>("fechaModificacion"));
        colTipoProducto.setCellValueFactory(new PropertyValueFactory<Productos, Integer>("codigoTipoProducto"));
        colProveedor.setCellValueFactory(new PropertyValueFactory<Productos, Integer>("codigoProveedor"));
    }
    
    public void activarControles(){
        txtNombreProducto.setEditable(true);
        txtExistencia.setDisable(true);
        txtPrecioCosto.setDisable(true);
        txtPrecioVenta.setDisable(true);
        txtProductoIVA.setDisable(true);
        cmbTipoProducto.setDisable(false);
        cmbProveedor.setDisable(false);
    }
    
    public void desactivarControles(){
        txtNombreProducto.setEditable(false);
        txtExistencia.setEditable(false);
        txtPrecioCosto.setEditable(false);
        txtPrecioVenta.setEditable(false);
        txtProductoIVA.setEditable(false);
        cmbTipoProducto.setDisable(true);
        cmbProveedor.setDisable(true);
    }
    
    public void limpiarControles(){
        txtNombreProducto.setText("");
        txtExistencia.setText("0");
        txtPrecioCosto.setText("0.00");
        txtPrecioVenta.setText("0.00");
        txtProductoIVA.setText("0.00");
        cmbTipoProducto.setValue("");
        cmbProveedor.setValue("");
    }
    
    
    /*public void generarReporte(){
        if(tblProductos.getSelectionModel().getSelectedItem() != null){
            Map parametros = new HashMap();
            int codProducto = ((Productos)tblProductos.getSelectionModel().getSelectedItem()).getCodigoProducto();
            parametros.put("_codProducto", codProducto);
            GenerarReporte.mostrarReporte("ReporteBuscarProducto.jasper", "Reporte de Productos", parametros);
        }else{
            int respuesta = JOptionPane.showConfirmDialog(null, "¿Desea ver el Reporte de todos los productos?","Ver reporte",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
            if(respuesta == JOptionPane.YES_OPTION){
                Map parametros = new HashMap();
                int codProducto = 0;
                parametros.put("", codProducto);
                GenerarReporte.mostrarReporte("ReporteProducto.jasper", "Reporte de Productos", parametros);
            }
        }
    }*/
    
    public Principal getEscenario() {
        return escenario;
    }

    public void setEscenario(Principal escenario) {
        this.escenario = escenario;
    }
    
    public void escenaMenu(){
        escenario.escenaMenu();
    }

}
