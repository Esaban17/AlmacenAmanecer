package org.estuardosaban.controladores;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import org.estuardosaban.sistema.Principal;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javax.swing.JOptionPane;
import org.estuardosaban.modelos.Usuarios;
import org.estuardosaban.db.Conexion;

public class UsuariosController implements Initializable {

    private enum operaciones{NUEVO, GUARDAR, ELIMINAR, ACTUALIZAR, NINGUNO}
    private Principal escenario;
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<Usuarios>listaUsuarios;    
    @FXML private TextField txtNombre;
    @FXML private TextField txtApellido;
    @FXML private TextField txtLogin;
    @FXML private PasswordField txtContraseña;
    @FXML private TableView tblUsuarios;
    @FXML private TableColumn colCodigo;
    @FXML private TableColumn colNombre;
    @FXML private TableColumn colApellido;
    @FXML private TableColumn colLogin;
    @FXML private TableColumn colContraseña;
    @FXML private ImageView imagen;
    @FXML private Button btnNuevo;
    @FXML private Button btnEditar;
    @FXML private Button btnEliminar;
    @FXML private Button btnRegresar;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
    }
    
    public void cargarDatos(){
        tblUsuarios.setItems(getUsuarios());
        colCodigo.setCellValueFactory(new PropertyValueFactory<Usuarios, Integer>("codigoUsuario"));
        colNombre.setCellValueFactory(new PropertyValueFactory<Usuarios, String>("nombreUsuario"));
        colApellido.setCellValueFactory(new PropertyValueFactory<Usuarios, String>("apellidoUsuario"));
        colLogin.setCellValueFactory(new PropertyValueFactory<Usuarios, String>("login"));
        colContraseña.setCellValueFactory(new PropertyValueFactory<Usuarios, String>("contrasena"));
        txtNombre.setEditable(false);
        txtApellido.setEditable(false);
        txtLogin.setEditable(false);
        txtContraseña.setEditable(false);
    }
    
    public ObservableList<Usuarios> getUsuarios(){
        ArrayList<Usuarios> lista = new ArrayList<Usuarios>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarUsuarios}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new Usuarios (resultado.getInt("codigoUsuario"),resultado.getString("nombreUsuario"),resultado.getString("apellidoUsuario"),resultado.getString("login"),resultado.getString("contrasena")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaUsuarios = FXCollections.observableArrayList(lista);
    }
    
    public void seleccionarElementos(){
        if(tblUsuarios.getSelectionModel().getSelectedIndex()>-1){
            txtNombre.setText(((Usuarios)tblUsuarios.getSelectionModel().getSelectedItem()).getNombreUsuario());
            txtApellido.setText(((Usuarios)tblUsuarios.getSelectionModel().getSelectedItem()).getApellidoUsuario());
            txtLogin.setText(((Usuarios)tblUsuarios.getSelectionModel().getSelectedItem()).getLogin());
            txtContraseña.setText(((Usuarios)tblUsuarios.getSelectionModel().getSelectedItem()).getContrasena());
        }
    }
    
    public Usuarios buscarUsuario(int codUsuario){
        Usuarios resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarUsuarios(?)}");
            procedimiento.setInt(1, codUsuario);
            ResultSet registro = procedimiento.executeQuery();
            while(registro.next()){
                resultado = new Usuarios(registro.getInt("codigoUsuario"),registro.getString("nombreUsuario"),registro.getString("apellidoUsuario"),registro.getString("login"),registro.getString("contrasena"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
   
    boolean ward = true;
    public void nuevo(){
        switch(tipoOperaciones){
            case NINGUNO:
                activarControles();
                limpiarControles();
                btnNuevo.setText("GUARDAR");
                btnEliminar.setText("CANCELAR");
                btnEditar.setDisable(true);
                tipoOperaciones=operaciones.GUARDAR;
            break;
            case GUARDAR:
                agregar();
                if(ward == true){
                    btnNuevo.setText("NUEVO");
                    btnEliminar.setText("ELIMINAR");
                    btnEditar.setDisable(false);
                    tipoOperaciones=operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    }

    public void editar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblUsuarios.getSelectionModel().getSelectedItem() != null){
                    btnEditar.setText("ACTUALIZAR");
                    btnEliminar.setText("CANCELAR");
                    btnNuevo.setDisable(true);
                    tipoOperaciones = operaciones.ACTUALIZAR;
                    txtNombre.setEditable(true);
                    txtApellido.setEditable(true);
                    txtLogin.setEditable(true);
                    txtContraseña.setEditable(true);
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Usuario");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(ward == true){
                    btnEditar.setText("EDITAR");
                    btnEliminar.setText("ELIMINAR");
                    btnNuevo.setDisable(false);
                    txtNombre.setEditable(false);
                    txtApellido.setEditable(false);
                    txtLogin.setEditable(false);
                    txtContraseña.setEditable(false);
                    tipoOperaciones = operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    } 

    public void eliminar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblUsuarios.getSelectionModel().getSelectedItem() != null){
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Usuario",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION){
                        borrar();
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Usuario");
                }
            break;
            default:
                cancelar();
            break;
        }
    } 
    
    public void agregar(){
        boolean entrar = true;
        try{
            if(!(txtNombre.getText().equals(""))&&!(txtApellido.getText().equals(""))&&!(txtLogin.getText().equals(""))&&!(txtContraseña.getText().equals(""))&&(entrar==true)){
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_InsertarUsuarios(?,?,?,?)}");
                Usuarios registro = new Usuarios();
                registro.setNombreUsuario(txtNombre.getText());
                registro.setApellidoUsuario(txtApellido.getText());
                registro.setLogin(txtLogin.getText());
                registro.setContrasena(txtContraseña.getText());
                procedimiento.setString(1, registro.getNombreUsuario());
                procedimiento.setString(2, registro.getApellidoUsuario());
                procedimiento.setString(3, registro.getLogin());
                procedimiento.setString(4, registro.getContrasena());
                procedimiento.execute();
                listaUsuarios.add(registro);
                ward = true;
            }else{
                if(entrar == true){
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    ward = false;
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public void actualizar(){
        boolean entrar = true;
        try{
            if(!(txtNombre.getText().equals(""))&&!(txtApellido.getText().equals(""))&&!(txtLogin.getText().equals(""))&&!(txtContraseña.getText().equals(""))&&(entrar==true)){
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ModificarUsuarios(?,?,?,?,?)}");
                Usuarios registro = (Usuarios)tblUsuarios.getSelectionModel().getSelectedItem();
                registro.setNombreUsuario(txtNombre.getText());
                registro.setApellidoUsuario(txtApellido.getText());
                registro.setLogin(txtLogin.getText());
                registro.setContrasena(txtContraseña.getText());
                procedimiento.setInt(1, registro.getCodigoUsuario());
                procedimiento.setString(2, registro.getNombreUsuario());
                procedimiento.setString(3, registro.getApellidoUsuario());
                procedimiento.setString(4, registro.getLogin());
                procedimiento.setString(5, registro.getContrasena());
                procedimiento.execute();
                ward = true;
            }else{
                if(entrar == true){
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    ward = false;
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void borrar(){
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_EliminarUsuarios(?)}");
            procedimiento.setInt(1, ((Usuarios)tblUsuarios.getSelectionModel().getSelectedItem()).getCodigoUsuario());
            procedimiento.execute();
            listaUsuarios.remove(tblUsuarios.getSelectionModel().getSelectedIndex());
            limpiarControles();
            cargarDatos();
            tipoOperaciones = operaciones.NINGUNO;
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void activarControles(){
        txtNombre.setEditable(true);
        txtApellido.setEditable(true);
        txtLogin.setEditable(true);
        txtContraseña.setEditable(true);
    }

    public void desactivarControles(){
        txtNombre.setEditable(false);
        txtApellido.setEditable(false);
        txtLogin.setEditable(false);
        txtContraseña.setEditable(false);
    }
    
    public void limpiarControles(){
        txtNombre.setText("");
        txtApellido.setText("");
        txtLogin.setText("");
        txtContraseña.setText("");
    }
    
    public void cancelar(){
        escenario.escenaProveedores();
    }
    
    public Principal getEscenario() {
        return escenario;
    }

    public void setEscenario(Principal escenario) {
        this.escenario = escenario;
    }
    
    public void escenaMenu() {
        this.escenario.escenaMenu();
    }
    
}
