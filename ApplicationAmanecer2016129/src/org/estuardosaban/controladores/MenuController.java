package org.estuardosaban.controladores;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javax.swing.JOptionPane;
import org.estuardosaban.db.Conexion;
import org.estuardosaban.sistema.Principal;

public class MenuController implements Initializable {
    Conexion cn = new Conexion();
    
    private Principal escenario;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
    
    public Principal getEscenario() {
        return escenario;
    }

    public void setEscenario(Principal escenario) {
        this.escenario = escenario;
    }
    
    public void escenaProveedores() {
        this.escenario.escenaProveedores();
    }
    
    public void escenaClientes() {
       this.escenario.escenaClientes();
    }
    
    public void escenaUsuarios() {
        this.escenario.escenaUsuarios();
    }
    
    public void escenaTipoProducto() {
        this.escenario.escenaTipoProducto();
    }
    
    public void escenaProductos() {
        this.escenario.escenaProductos();
    }
    
    public void escenaCompras() {
        this.escenario.escenaCompras();
    }
    
    public void escenaLogin() {
        this.escenario.escenaLogin();
    }
    
    public void escenaVentas() {
        this.escenario.escenaVentas();
    }
    public void escenaDetalleVentas() {
        this.escenario.escenaDetalleVentas();
    }
    
}
