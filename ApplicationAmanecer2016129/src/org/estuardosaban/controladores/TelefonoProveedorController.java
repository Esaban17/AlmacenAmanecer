package org.estuardosaban.controladores;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.JOptionPane;

import org.estuardosaban.sistema.Principal;
import org.estuardosaban.db.Conexion;
import org.estuardosaban.modelos.Proveedores;
import org.estuardosaban.modelos.TelefonoProveedor;

public class TelefonoProveedorController implements Initializable {
    
    private enum operaciones{NUEVO, GUARDAR, ELIMINAR, ACTUALIZAR, NINGUNO}
    private Principal escenario;
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<TelefonoProveedor>listaTelefonoProveedor; 
    private ObservableList<Proveedores>listaProveedores;   
    
    @FXML private TextField txtTelefonoProveedor;
    @FXML private TextField txtDescripcion;
    @FXML private ComboBox cmbProveedor;
    
    @FXML private TableView tblTelefonoProveedor;
    @FXML private TableColumn colCodigo;
    @FXML private TableColumn colTelefonoProveedor;
    @FXML private TableColumn colDescripcion;
    @FXML private TableColumn colProveedor;
    
    @FXML private Button btnNuevo;
    @FXML private Button btnEditar;
    @FXML private Button btnEliminar;
    @FXML private Button btnRegresar;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cmbProveedor.setItems(getProveedores());
        desactivarControles();
        cargarDatos();        
    }
    public Proveedores buscarProveedor(int codProveedor){
        Proveedores resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarProveedores(?)}");
            procedimiento.setInt(1, codProveedor);
            ResultSet registro= procedimiento.executeQuery();
            while (registro.next()){
                resultado = new Proveedores (registro.getInt("codigoProveedor"),registro.getString("razonSocial"),registro.getString("nit"),registro.getString("direccionProveedor"),registro.getString("paginaWeb"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }

    public TelefonoProveedor buscarTelefonoProveedor(int codTelefonoProveedor){
        TelefonoProveedor resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarTelefonoProveedor(?)}");
            procedimiento.setInt(1, codTelefonoProveedor);
            ResultSet registro = procedimiento.executeQuery();
            while(registro.next()){
                resultado = new TelefonoProveedor(registro.getInt("codigoTelefonoProveedor"), registro.getString("numero"),registro.getString("descripcion"),registro.getInt("codigoProveedor"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    public ObservableList<Proveedores> getProveedores(){
        ArrayList<Proveedores> lista = new ArrayList<Proveedores>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarProveedores}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new Proveedores (resultado.getInt("codigoProveedor"),resultado.getString("razonSocial"),resultado.getString("nit"),resultado.getString("direccionProveedor"),resultado.getString("paginaWeb")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaProveedores = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<TelefonoProveedor> getTelefonoProveedor(){
        ArrayList<TelefonoProveedor> lista = new ArrayList<TelefonoProveedor>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarTelefonoProveedor}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new TelefonoProveedor (resultado.getInt("codigoTelefonoProveedor"),resultado.getString("numero"),resultado.getString("descripcion"),resultado.getInt("codigoProveedor")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaTelefonoProveedor = FXCollections.observableArrayList(lista);
    }
    
    boolean ward = true;
    public void nuevo(){
        switch(tipoOperaciones){
            case NINGUNO:
                activarControles();
                limpiarControles();
                btnNuevo.setText("GUARDAR");
                btnEliminar.setText("CANCELAR");
                btnEditar.setDisable(true);
                tipoOperaciones=operaciones.GUARDAR;
            break;
            case GUARDAR:
                agregar();
                if(ward == true){
                    btnNuevo.setText("NUEVO");
                    btnEliminar.setText("ELIMINAR");
                    btnEditar.setDisable(false);
                    tipoOperaciones=operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    }
    public void editar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblTelefonoProveedor.getSelectionModel().getSelectedItem() != null){
                    btnEditar.setText("ACTUALIZAR");
                    btnEliminar.setText("CANCELAR");
                    btnNuevo.setDisable(true);
                    tipoOperaciones = operaciones.ACTUALIZAR;
                    txtTelefonoProveedor.setEditable(true);
                    txtDescripcion.setEditable(true);
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Telefono");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(ward == true){
                    btnEditar.setText("EDITAR");
                    btnEliminar.setText("ELIMINAR");
                    btnNuevo.setDisable(false);
                    txtTelefonoProveedor.setEditable(false);
                    txtDescripcion.setEditable(false);
                    tipoOperaciones = operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    }
    
    public void eliminar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblTelefonoProveedor.getSelectionModel().getSelectedItem() != null){
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Telefono Proveedor",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION){
                        borrar();
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Telefono");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void cancelar(){
        escenario.escenaProveedores();
    }
    
    public void agregar(){
        try{
            if(!(txtTelefonoProveedor.getText().equals(""))&&!(txtDescripcion.getText().equals(""))&&!cmbProveedor.getValue().equals("")){
                TelefonoProveedor registro = new TelefonoProveedor();
                registro.setNumero(txtTelefonoProveedor.getText());
                registro.setDescripcion(txtDescripcion.getText());
                registro.setCodigoProveedor(((Proveedores)cmbProveedor.getSelectionModel().getSelectedItem()).getCodigoProveedor());
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_InsertarTelefonoProveedor(?,?,?)}");
                procedimiento.setString(1, registro.getNumero());
                procedimiento.setString(2, registro.getDescripcion());
                procedimiento.setInt(3, registro.getCodigoProveedor());
                procedimiento.execute();
                listaTelefonoProveedor.add(registro);
                limpiarControles();
                ward = true;
            }else{
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                ward = false;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    public void actualizar(){
        try{
            if(!(txtTelefonoProveedor.getText().equals(""))&&!(txtDescripcion.getText().equals(""))&&!(cmbProveedor.getValue().equals(""))){
                TelefonoProveedor registro = (TelefonoProveedor) tblTelefonoProveedor.getSelectionModel().getSelectedItem();
                registro.setNumero(txtTelefonoProveedor.getText());
                registro.setDescripcion(txtDescripcion.getText());
                registro.setCodigoProveedor(((Proveedores)cmbProveedor.getSelectionModel().getSelectedItem()).getCodigoProveedor());
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ModificarTelefonoProveedor(?,?,?,?)}");
                procedimiento.setInt(1,registro.getCodigoTelefonoProveedor());
                procedimiento.setString(2, registro.getNumero());
                procedimiento.setString(3, registro.getDescripcion());
                procedimiento.setInt(4, registro.getCodigoProveedor());
                procedimiento.execute();
                listaTelefonoProveedor.add(registro);
                ward = true;
                limpiarControles();
            }else{
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                ward = false;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void borrar(){
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_EliminarTelefonoProveedor(?)}");
            procedimiento.setInt(1, ((TelefonoProveedor)tblTelefonoProveedor.getSelectionModel().getSelectedItem()).getCodigoTelefonoProveedor());
            procedimiento.execute();
            listaTelefonoProveedor.remove(tblTelefonoProveedor.getSelectionModel().getSelectedIndex());
            limpiarControles();
            cargarDatos();
            tipoOperaciones = operaciones.NINGUNO;
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void seleccionarElementos(){
        if(tblTelefonoProveedor.getSelectionModel().getSelectedIndex()>-1){
            txtTelefonoProveedor.setText(((TelefonoProveedor)tblTelefonoProveedor.getSelectionModel().getSelectedItem()).getNumero());
            txtDescripcion.setText(String.valueOf(((TelefonoProveedor)tblTelefonoProveedor.getSelectionModel().getSelectedItem()).getDescripcion()));                     
            cmbProveedor.getSelectionModel().select(buscarProveedor(((TelefonoProveedor)tblTelefonoProveedor.getSelectionModel().getSelectedItem()).getCodigoProveedor()));
        }
    }

    public void cargarDatos(){
        desactivarControles();
        tblTelefonoProveedor.setItems(getTelefonoProveedor());
        colCodigo.setCellValueFactory(new PropertyValueFactory<TelefonoProveedor, Integer>("codigoTelefonoProveedor"));
        colTelefonoProveedor.setCellValueFactory(new PropertyValueFactory<TelefonoProveedor, String>("numero"));
        colDescripcion.setCellValueFactory(new PropertyValueFactory<TelefonoProveedor, String>("descripcion"));
        colProveedor.setCellValueFactory(new PropertyValueFactory<TelefonoProveedor, Integer>("codigoProveedor"));
    }
    
    public void activarControles(){
        txtTelefonoProveedor.setEditable(true);
        txtDescripcion.setEditable(true);
        cmbProveedor.setDisable(false);
    }
    
    public void desactivarControles(){
        txtTelefonoProveedor.setEditable(false);
        txtDescripcion.setEditable(false);
        cmbProveedor.setDisable(true);
    }
    
    public void limpiarControles(){
        txtTelefonoProveedor.setText("");
        txtDescripcion.setText("");
        cmbProveedor.setValue("");
    }
    
    public Principal getEscenario() {
        return escenario;
    }

    public void setEscenario(Principal escenario) {
        this.escenario = escenario;
    }
    
    public void escenaProveedores(){
        escenario.escenaProveedores();
    }
}