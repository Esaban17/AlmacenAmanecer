package org.estuardosaban.controladores;
import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.StringConverter;
import javax.swing.JOptionPane;
import java.util.HashMap;
import java.util.Map;
import org.estuardosaban.sistema.Principal;
import org.estuardosaban.modelos.Compras;
import org.estuardosaban.modelos.Proveedores;
import org.estuardosaban.db.Conexion;
import org.estuardosaban.modelos.Productos;
public class ComprasController implements Initializable {
    
    private enum operaciones{NUEVO, GUARDAR, ELIMINAR, ACTUALIZAR, NINGUNO}
    private Principal escenario;
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<Compras> listaCompras;
    private ObservableList<Productos> listaProductos;
    private ObservableList<Proveedores> listaProveedores;
    
    @FXML private ComboBox cmbProveedor;
    @FXML private ComboBox cmbProducto;
    @FXML private TextField txtDireccion;
    @FXML private TextField txtExistencia;
    @FXML private TextField txtCantidad;
    @FXML private TextField txtPrecioUnitario;
    @FXML private TextField txtTotal;
    
    @FXML private TableView tblCompras;
    @FXML private TableColumn colCodigo;
    @FXML private TableColumn colProveedor;
    @FXML private TableColumn colProducto;
    @FXML private TableColumn colDireccion;
    @FXML private TableColumn colPrecioUnitario;
    @FXML private TableColumn colCantidad;
    @FXML private TableColumn colTotal;
    
    @FXML private Button btnRegresar;
    @FXML private Button btnNuevo;
    @FXML private Button btnEditar;
    @FXML private Button btnEliminar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
        desactivarControles();
    }
    public void cargarDatos(){
        tblCompras.setItems(getCompras());
        colCodigo.setCellValueFactory(new PropertyValueFactory<Compras, Integer>("codigoCompra"));
        colProveedor.setCellValueFactory(new PropertyValueFactory<Compras, Integer>("codigoProveedor"));
        colProducto.setCellValueFactory(new PropertyValueFactory<Compras, Integer>("codigoProducto"));
        colDireccion.setCellValueFactory(new PropertyValueFactory<Compras, String>("direccion"));
        colPrecioUnitario.setCellValueFactory(new PropertyValueFactory<Compras, Double>("precioUnitario"));
        colCantidad.setCellValueFactory(new PropertyValueFactory<Compras, Integer>("cantidad"));
        colTotal.setCellValueFactory(new PropertyValueFactory<Compras, Double>("total"));
        cmbProveedor.setItems(getProveedores());
        cmbProducto.setItems(getProductos());
    }
    public ObservableList<Compras> getCompras(){
        ArrayList<Compras> lista = new ArrayList<Compras>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarCompras}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new Compras (resultado.getInt("codigoCompra"),resultado.getInt("codigoProveedor"),resultado.getInt("codigoProducto"),resultado.getString("direccion"),resultado.getDouble("precioUnitario"),resultado.getInt("cantidad"),resultado.getDouble("total")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaCompras = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<Proveedores> getProveedores(){
        ArrayList<Proveedores> lista = new ArrayList<Proveedores>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarProveedores}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new Proveedores (resultado.getInt("codigoProveedor"),resultado.getString("razonSocial"),resultado.getString("nit"),resultado.getString("direccionProveedor"),resultado.getString("paginaWeb")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaProveedores = FXCollections.observableArrayList(lista);
    }
    public ObservableList<Productos> getProductos(){
        ArrayList<Productos> lista = new ArrayList<Productos>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ConsultarProductos}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()){
                lista.add(new Productos (resultado.getInt("codigoProducto"),resultado.getString("nombreProducto"),resultado.getInt("existencia"),resultado.getDouble("precioCosto"),resultado.getDouble("precioVenta"),resultado.getDouble("productoIVA"),resultado.getDate("fechaCreacion"),resultado.getDate("fechaModificacion"),resultado.getInt("codigoTipoProducto"),resultado.getInt("codigoProveedor")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return listaProductos = FXCollections.observableArrayList(lista);
    }
    
    public void seleccionarElementos(){
        if(tblCompras.getSelectionModel().getSelectedIndex()>-1){
            cmbProveedor.getSelectionModel().select(buscarProveedor(((Compras)tblCompras.getSelectionModel().getSelectedItem()).getCodigoProveedor()));
            cmbProducto.getSelectionModel().select(buscarProducto(((Compras)tblCompras.getSelectionModel().getSelectedItem()).getCodigoProducto()));
            txtDireccion.setText(((Compras)tblCompras.getSelectionModel().getSelectedItem()).getDireccion());
            txtPrecioUnitario.setText(String.valueOf(((Compras)tblCompras.getSelectionModel().getSelectedItem()).getPrecioUnitario()));
            txtCantidad.setText(String.valueOf(((Compras)tblCompras.getSelectionModel().getSelectedItem()).getCantidad()));
            txtTotal.setText(String.valueOf(((Compras)tblCompras.getSelectionModel().getSelectedItem()).getTotal()));
        }
    }
    
    public Compras buscarCompra(int codCompra){
        Compras resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarCompras(?)}");
            procedimiento.setInt(1, codCompra);
            ResultSet registro = procedimiento.executeQuery();
            while(registro.next()){
                resultado = new Compras(registro.getInt("codigoCompra"),registro.getInt("codigoProveedor"),registro.getInt("codigoProducto"), registro.getString("direccion"),registro.getDouble("precioUnitario"),registro.getInt("cantidad"),registro.getDouble("total"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    public Proveedores buscarProveedor(int codProveedor){
        Proveedores resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarProveedores(?)}");
            procedimiento.setInt(1, codProveedor);
            ResultSet registro = procedimiento.executeQuery();
            while(registro.next()){
                resultado = new Proveedores(registro.getInt("codigoProveedor"),registro.getString("razonSocial"),registro.getString("nit"),registro.getString("direccionProveedor"),registro.getString("paginaWeb"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    } 
    public Productos buscarProducto(int codigoProducto){
        Productos resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_BuscarProductos(?)}");
            procedimiento.setInt(1, codigoProducto);
            ResultSet registro= procedimiento.executeQuery();
            while (registro.next()){
                resultado = new Productos (registro.getInt("codigoProducto"), registro.getString("nombreProducto"),registro.getInt("existencia"),registro.getDouble("precioCosto"),registro.getDouble("precioVenta"),registro.getDouble("productoIva"),registro.getDate("fechaCreacion"),registro.getDate("fechaModificacion"),registro.getInt("codigoTipoProducto"),registro.getInt("codigoProveedor"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    public void getProducto() {
        try{
            if(((cmbProducto.getSelectionModel().getSelectedItem() != ""))) {
                Productos producto = (Productos)cmbProducto.getSelectionModel().getSelectedItem();
                txtExistencia.setText(String.valueOf(producto.getExistencia()));
                txtPrecioUnitario.setText(String.valueOf(producto.getPrecioCosto()));
            }            
        }catch(Exception e){
            
        }
    }
    
    public void getTotal() {
        try{
            if(cmbProducto.getValue().equals("")) {
            JOptionPane.showMessageDialog(null, "Debes seleccionar una Producto");
            txtTotal.setText("0.00");
        }else{
            double total;
            double precioUnitario;
            double cantidad;
            
            try{
                cantidad = Double.parseDouble(txtCantidad.getText());
                precioUnitario = Double.parseDouble(txtPrecioUnitario.getText());
            }catch(Exception e){
                precioUnitario = 0.00;
                cantidad = 0.00;
            }
            total = precioUnitario * cantidad;
            txtTotal.setText(String.valueOf(total));
        }
        }catch(Exception e) {
            
        }
    }

    
    boolean ward = true;
    public void nuevo(){
        switch(tipoOperaciones){
            case NINGUNO:
                activarControles();
                limpiarControles();
                btnNuevo.setText("GUARDAR");
                btnEliminar.setText("CANCELAR");
                btnEditar.setDisable(true);
                tipoOperaciones=operaciones.GUARDAR;
            break;
            case GUARDAR:
                agregar();
                if(ward == true){
                    btnNuevo.setText("NUEVO");
                    btnEliminar.setText("ELIMINAR");
                    btnEditar.setDisable(false);
                    tipoOperaciones=operaciones.NINGUNO;
                    cargarDatos();
                }
            break;
        }
    }
    public void editar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblCompras.getSelectionModel().getSelectedItem() != null){
                    btnEditar.setText("ACTUALIZAR");
                    btnEliminar.setText("CANCELAR");
                    btnNuevo.setDisable(true);
                    tipoOperaciones = operaciones.ACTUALIZAR;
                    txtDireccion.setEditable(true);
                    txtCantidad.setEditable(true);
                    txtPrecioUnitario.setEditable(true);
                    cmbProveedor.setDisable(false);
                    cmbProducto.setDisable(false);
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar una Compra");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(ward == true){
                    btnEditar.setText("EDITAR");
                    btnEliminar.setText("ELIMINAR");
                    btnNuevo.setDisable(false);
                    txtDireccion.setEditable(true);
                    txtCantidad.setEditable(true);
                    tipoOperaciones = operaciones.NINGUNO;
                    cmbProveedor.setDisable(true);
                    cmbProducto.setDisable(true);
                    cargarDatos();
                }
            break;
        }
    }
    public void eliminar(){
        switch(tipoOperaciones){
            case NINGUNO:
                if(tblCompras.getSelectionModel().getSelectedItem() != null){
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Compra",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION){
                        borrar();
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar una Compra");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void activarControles(){
        txtExistencia.setEditable(false);
        txtDireccion.setEditable(true);
        txtCantidad.setEditable(true);
        txtPrecioUnitario.setEditable(true);
        cmbProveedor.setDisable(false);        
        cmbProducto.setDisable(false);
    }
    
    public void desactivarControles(){
        txtDireccion.setEditable(false);
        txtExistencia.setEditable(false);
        txtCantidad.setEditable(false);
        txtPrecioUnitario.setEditable(false);
        txtTotal.setEditable(false);
        cmbProveedor.setDisable(true);
        cmbProducto.setDisable(true);
    }
    
    public void limpiarControles(){
        txtDireccion.setText("");
        txtExistencia.setText("0");
        txtPrecioUnitario.setText("0.00");
        txtCantidad.setText("0");
        txtTotal.setText("0.00");
        cmbProducto.setValue("");
        cmbProveedor.setValue("");
    }

    public void agregar(){
        try{
            if(!(txtDireccion.getText().equals(""))&&!(txtCantidad.getText().equals(""))&&!(cmbProducto.getValue().equals(""))&&!(cmbProveedor.getValue().equals(""))){
                Compras registro = new Compras();
                registro.setDireccion(txtDireccion.getText());
                registro.setPrecioUnitario(Double.parseDouble(txtPrecioUnitario.getText()));
                registro.setCantidad(Integer.parseInt(txtCantidad.getText()));
                registro.setTotal(Double.parseDouble(txtTotal.getText()));
                registro.setCodigoProveedor(((Proveedores)cmbProveedor.getSelectionModel().getSelectedItem()).getCodigoProveedor());
                registro.setCodigoProducto(((Productos)cmbProducto.getSelectionModel().getSelectedItem()).getCodigoProducto());
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_InsertarCompras(?,?,?,?,?,?)}");
                procedimiento.setInt(1, registro.getCodigoProveedor());
                procedimiento.setInt(2, registro.getCodigoProducto());
                procedimiento.setString(3, registro.getDireccion());
                procedimiento.setDouble(4, registro.getPrecioUnitario());
                procedimiento.setInt(5, registro.getCantidad());
                procedimiento.setDouble(6, registro.getTotal());
                procedimiento.execute();
                listaCompras.add(registro);
                ward = true;
            }else{
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                ward = false;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    public void actualizar(){
        try{
            if(!(txtDireccion.getText().equals(""))&&!(txtCantidad.getText().equals(""))&&!(cmbProducto.getValue().equals(""))&&!(cmbProveedor.getValue().equals(""))){
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_ModificarCompras(?,?,?,?,?,?,?)}");
                Compras registro = (Compras)tblCompras.getSelectionModel().getSelectedItem();
                registro.setDireccion(txtDireccion.getText());
                registro.setCantidad(Integer.parseInt(txtCantidad.getText()));
                registro.setPrecioUnitario(Double.parseDouble(txtPrecioUnitario.getText()));
                registro.setTotal(Double.parseDouble(txtTotal.getText()));
                registro.setCodigoProveedor(((Proveedores)cmbProveedor.getSelectionModel().getSelectedItem()).getCodigoProveedor());
                registro.setCodigoProducto(((Productos)cmbProducto.getSelectionModel().getSelectedItem()).getCodigoProducto());
                procedimiento.setInt(1, registro.getCodigoCompra());
                procedimiento.setInt(2, registro.getCodigoProveedor());
                procedimiento.setInt(3, registro.getCodigoProducto());
                procedimiento.setString(4, registro.getDireccion());
                procedimiento.setDouble(5, registro.getPrecioUnitario());
                procedimiento.setInt(6, registro.getCantidad());
                procedimiento.setDouble(7, registro.getTotal());
                procedimiento.execute();
                ward = true;
            }else{
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                ward = false;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    public void borrar(){
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_EliminarCompras(?)}");
            procedimiento.setInt(1, ((Compras)tblCompras.getSelectionModel().getSelectedItem()).getCodigoCompra());
            procedimiento.execute();
            listaCompras.remove(tblCompras.getSelectionModel().getSelectedIndex());
            limpiarControles();
            cancelar();
            tipoOperaciones = operaciones.NINGUNO;
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    public void cancelar(){
        escenario.escenaCompras();
    }

    public Principal getEscenario() {
        return escenario;
    }

    public void setEscenario(Principal escenario) {
        this.escenario = escenario;
    }
    
    public void escenaMenu(){
        this.escenario.escenaMenu();
    }
    
    
}
