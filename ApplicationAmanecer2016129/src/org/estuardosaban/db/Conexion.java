package org.estuardosaban.db;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import com.microsoft.sqlserver.jdbc.SQLServerDriver;

public class Conexion {
    private Connection conexion;
    private Statement sentencia;
    private static Conexion instancia;
    
    public Conexion(){
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            conexion=DriverManager.getConnection("jdbc:sqlserver://localhost:1433;instanceName=MSSQLSERVER;dataBaseName=DBAmanecer2016129;user=sa;password=sa123");
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }catch(InstantiationException e){
            e.printStackTrace();
        }catch(IllegalAccessException e){
            e.printStackTrace();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        Conexion pruebaCn = Conexion.getInstancia();
        if(pruebaCn != null) {
            System.out.println("Conectado");
            System.out.println(""+pruebaCn);
        }else {
            System.out.println("Desconectado");
        }
    }
    
    public static Conexion getInstancia(){
        if (instancia==null){
            instancia = new Conexion();
        }
        return instancia;
    }

    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }
}
