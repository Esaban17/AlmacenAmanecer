package org.estuardosaban.modelos;

import java.util.Date;

public class Factura {
    private int codigoFactura;
    private int numeroFactura;
    private Date fecha;
    private String nitCliente;
    private int cantidad;
    private double precioVenta;
    private int codigoProducto;
    private double total;
    private int codigoCliente;

    public Factura() {
    
    }

    public Factura(int codigoFactura, int numeroFactura, Date fecha, String nitCliente, int cantidad, double precioVenta, int codigoProducto, double total, int codigoCliente) {
        this.codigoFactura = codigoFactura;
        this.numeroFactura = numeroFactura;
        this.fecha = fecha;
        this.nitCliente = nitCliente;
        this.cantidad = cantidad;
        this.precioVenta = precioVenta;
        this.codigoProducto = codigoProducto;
        this.total = total;
        this.codigoCliente = codigoCliente;
    }

    public int getCodigoFactura() {
        return codigoFactura;
    }

    public void setCodigoFactura(int codigoFactura) {
        this.codigoFactura = codigoFactura;
    }

    public int getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(int numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNitCliente() {
        return nitCliente;
    }

    public void setNitCliente(String nitCliente) {
        this.nitCliente = nitCliente;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }
    
    public String toString(){
        return getCodigoFactura() + " - " + getNumeroFactura();
    }
    
}
