package org.estuardosaban.modelos;

public class TelefonoCliente {
    private int codigoTelefonoCliente;
    private String telefono;
    private String descripcion;
    private int codigoCliente;

    public TelefonoCliente() {
   
    }

    public TelefonoCliente(int codigoTelefonoCliente, String telefono, String descripcion, int codigoCliente) {
        this.codigoTelefonoCliente = codigoTelefonoCliente;
        this.telefono = telefono;
        this.descripcion = descripcion;
        this.codigoCliente = codigoCliente;
    }

    public int getCodigoTelefonoCliente() {
        return codigoTelefonoCliente;
    }

    public void setCodigoTelefonoCliente(int codigoTelefonoCliente) {
        this.codigoTelefonoCliente = codigoTelefonoCliente;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }
    
    public String toString(){
        return getCodigoTelefonoCliente() + " - " + getTelefono();
    }
    
}
