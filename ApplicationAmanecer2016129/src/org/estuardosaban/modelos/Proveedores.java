package org.estuardosaban.modelos;

public class Proveedores {
    private int codigoProveedor;
    private String razonSocial;
    private String nit;
    private String direccionProveedor;
    private String paginaWeb;

    public Proveedores() {

    }

    public Proveedores(int codigoProveedor, String razonSocial, String nit, String direccionProveedor, String paginaWeb) {
        this.codigoProveedor = codigoProveedor;
        this.razonSocial = razonSocial;
        this.nit = nit;
        this.direccionProveedor = direccionProveedor;
        this.paginaWeb = paginaWeb;
    }

    public int getCodigoProveedor() {
        return codigoProveedor;
    }

    public void setCodigoProveedor(int codigoProveedor) {
        this.codigoProveedor = codigoProveedor;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getDireccionProveedor() {
        return direccionProveedor;
    }

    public void setDireccionProveedor(String direccionProveedor) {
        this.direccionProveedor = direccionProveedor;
    }

    public String getPaginaWeb() {
        return paginaWeb;
    }

    public void setPaginaWeb(String paginaWeb) {
        this.paginaWeb = paginaWeb;
    }
    
    public String toString(){
        return getCodigoProveedor() + " - " + getRazonSocial();
    }    
    
}
