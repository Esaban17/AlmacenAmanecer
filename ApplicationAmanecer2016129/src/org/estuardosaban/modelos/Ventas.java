package org.estuardosaban.modelos;

import java.util.Date;

public class Ventas {
    private int codigoVenta;
    private Date fechaVenta;
    private double iva;
    private String observacion;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private double total;
    private int numeroFactura;
    private int codigoCliente;
    private int codigoFormaPago;
    private int codigoTipoProducto;
    private int codigoUsuario;


    public Ventas() {
    }

    public Ventas(int codigoVenta, Date fechaVenta, double iva, String observacion, Date fechaCreacion, Date fechaModificacion, double total, int numeroFactura, int codigoCliente, int codigoFormaPago, int codigoTipoProducto, int codigoUsuario) {
        this.codigoVenta = codigoVenta;
        this.fechaVenta = fechaVenta;
        this.iva = iva;
        this.observacion = observacion;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.total = total;
        this.numeroFactura = numeroFactura;
        this.codigoCliente = codigoCliente;
        this.codigoFormaPago = codigoFormaPago;
        this.codigoTipoProducto = codigoTipoProducto;
        this.codigoUsuario = codigoUsuario;
    }

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
    }

    public Date getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(Date fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(int numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public int getCodigoFormaPago() {
        return codigoFormaPago;
    }

    public void setCodigoFormaPago(int codigoFormaPago) {
        this.codigoFormaPago = codigoFormaPago;
    }

    public int getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    public void setCodigoTipoProducto(int codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
    }

    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String toString(){
        return getCodigoVenta() + "";
    }
    
}
