package org.estuardosaban.modelos;

public class FormaPago {
    private int codigoFormaPago;
    private String descripcion;
    
    public FormaPago() {
    }

    public FormaPago(int codigoFormaPago, String descripcion) {
        this.codigoFormaPago = codigoFormaPago;
        this.descripcion = descripcion;
    }

    public int getCodigoFormaPago() {
        return codigoFormaPago;
    }

    public void setCodigoFormaPago(int codigoFormaPago) {
        this.codigoFormaPago = codigoFormaPago;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String toString(){
        return getCodigoFormaPago() + " - " + getDescripcion();
    }
}
