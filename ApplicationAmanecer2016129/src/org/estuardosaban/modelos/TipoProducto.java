package org.estuardosaban.modelos;

import java.util.Date;

public class TipoProducto {
    private int codigoTipoProducto;
    private String descripcion;
    private Date fechaCreacion;
    private Date fechaModificacion;

    public TipoProducto() {
    
    }

    public TipoProducto(int codigoTipoProducto, String descripcion, Date fechaCreacion, Date fechaModificacion) {
        this.codigoTipoProducto = codigoTipoProducto;
        this.descripcion = descripcion;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
    }

    public int getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    public void setCodigoTipoProducto(int codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    public String toString(){
        return getCodigoTipoProducto() + " - " + getDescripcion();
    }

    
}
