package org.estuardosaban.modelos;

public class DetalleVentas {
    private int  codigoDetalleVenta;
    private int cantidad;
    private double precioVenta;
    private double productoIVA;
    private int codigoVenta;
    private int codigoProducto;

    public DetalleVentas() {
    
    }

    public DetalleVentas(int codigoDetalleVenta, int cantidad, double precioVenta, double productoIVA, int codigoVenta, int codigoProducto) {
        this.codigoDetalleVenta = codigoDetalleVenta;
        this.cantidad = cantidad;
        this.precioVenta = precioVenta;
        this.productoIVA = productoIVA;
        this.codigoVenta = codigoVenta;
        this.codigoProducto = codigoProducto;
    }

    public int getCodigoDetalleVenta() {
        return codigoDetalleVenta;
    }

    public void setCodigoDetalleVenta(int codigoDetalleVenta) {
        this.codigoDetalleVenta = codigoDetalleVenta;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public double getProductoIVA() {
        return productoIVA;
    }

    public void setProductoIVA(double productoIVA) {
        this.productoIVA = productoIVA;
    }

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
    }

    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }
    
    public String toString(){
        return getCodigoDetalleVenta() + "";
    }
}
