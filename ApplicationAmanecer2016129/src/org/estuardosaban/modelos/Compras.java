package org.estuardosaban.modelos;

public class Compras {
    private int codigoCompra;
    private int codigoProveedor;
    private int codigoProducto;
    private String direccion;
    private double precioUnitario;
    private int cantidad;
    private double total;

    public Compras() {
        
    }

    public Compras(int codigoCompra, int codigoProveedor, int codigoProducto, String direccion, double precioUnitario, int cantidad, double total) {
        this.codigoCompra = codigoCompra;
        this.codigoProveedor = codigoProveedor;
        this.codigoProducto = codigoProducto;
        this.direccion = direccion;
        this.precioUnitario = precioUnitario;
        this.cantidad = cantidad;
        this.total = total;
    }

    public int getCodigoCompra() {
        return codigoCompra;
    }

    public void setCodigoCompra(int codigoCompra) {
        this.codigoCompra = codigoCompra;
    }

    public int getCodigoProveedor() {
        return codigoProveedor;
    }

    public void setCodigoProveedor(int codigoProveedor) {
        this.codigoProveedor = codigoProveedor;
    }

    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
    public String toString(){
        return getCodigoCompra() + "";
    }
    

}
