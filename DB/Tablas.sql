CREATE DATABASE DBAmanecer2016129

USE DBAmanecer2016129
GO

CREATE TABLE Usuarios(
	codigoUsuario INT NOT NULL PRIMARY KEY IDENTITY,
	nombreUsuario VARCHAR(50) NOT NULL,
	apellidoUsuario VARCHAR(50) NOT NULL,
	login VARCHAR(128) NOT NULL,
	contrasena VARCHAR(256) NOT NULL
)
GO

CREATE TABLE TipoProducto(
	codigoTipoProducto INT NOT NULL PRIMARY KEY IDENTITY,
	descripcion VARCHAR(128),
	fechaCreacion DATE NOT NULL,
	fechaModificacion DATE
)
GO

CREATE TABLE Proveedores(
	codigoProveedor INT NOT NULL PRIMARY KEY IDENTITY,
	razonSocial VARCHAR(128) NOT NULL,
	nit VARCHAR(12) NOT NULL,
	direccionProveedor VARCHAR(128) NOT NULL,
	paginaWeb VARCHAR(200),
)
GO

CREATE TABLE Clientes(
	codigoCliente INT NOT NULL PRIMARY KEY IDENTITY,
	nombreCliente VARCHAR(40) NOT NULL,
	apellidoCliente VARCHAR(40) NOT NULL,
	direccionCliente VARCHAR(128),
	nitCliente VARCHAR(128) NOT NULL,
	fechaCreacion DATE NOT NULL,
	fechaModificacion DATE,
)
GO

CREATE TABLE TelefonoCliente(
	codigoTelefonoCliente INT NOT NULL PRIMARY KEY IDENTITY,
	telefono VARCHAR(8) NOT NULL,
	descripcion VARCHAR(25),
	codigoCliente INT NOT NULL,
	FOREIGN KEY (codigoCliente) REFERENCES Clientes(codigoCliente)
)
GO

CREATE TABLE Email_Cliente(
	codigoEmailCliente INT NOT NULL PRIMARY KEY IDENTITY,
	email VARCHAR(256),
	descripcion VARCHAR(128),
	codigoCliente INT NOT NULL,
	FOREIGN KEY (codigoCliente) REFERENCES Clientes(codigoCliente)
)
GO

CREATE TABLE FormaPago(
	codigoFormaPago INT NOT NULL PRIMARY KEY IDENTITY,
	descripcion VARCHAR(128)
)
GO

CREATE TABLE TelefonoProveedor(
	codigoTelefonoProveedor INT NOT NULL PRIMARY KEY IDENTITY,
	numero VARCHAR(8) NOT NULL,
	descripcion VARCHAR(30),
	codigoProveedor INT,
	FOREIGN KEY (codigoProveedor) REFERENCES Proveedores(codigoProveedor)
)
GO

CREATE TABLE Productos(
	codigoProducto INT NOT NULL PRIMARY KEY IDENTITY,
	nombreProducto VARCHAR(128) NOT NULL,
	existencia INT NOT NULL,
	precioCosto DECIMAL(10,2) DEFAULT 0.00,
	precioVenta DECIMAL(10,2) DEFAULT 0.00,
	productoIVA DECIMAL(10,2) DEFAULT 0.00,
	fechaCreacion DATE NOT NULL,
	fechaModificacion DATE,
	codigoTipoProducto INT NOT NULL,
	codigoProveedor INT NOT NULL,
	FOREIGN KEY (codigoTipoProducto) REFERENCES TipoProducto(codigoTipoProducto),
	FOREIGN KEY (codigoProveedor) REFERENCES Proveedores(codigoProveedor)
)
GO

CREATE TABLE Factura(
	codigoFactura INT NOT NULL PRIMARY KEY IDENTITY,
	numeroFactura INT NOT NULL,
	fecha DATE NOT NULL,
	nitCliente VARCHAR(128) NOT NULL,
	cantidad INT NOT NULL,
	precioVenta DECIMAL(10,2) DEFAULT 0.00,
	codigoProducto INT NOT NULL,
	total DECIMAL(10,2) DEFAULT 0.00,
	codigoCliente INT NOT NULL,
	FOREIGN KEY (codigoProducto) REFERENCES Productos(codigoProducto),
	FOREIGN KEY (codigoCliente) REFERENCES Clientes(codigoCliente)
)
GO

CREATE TABLE Compras(
	codigoCompra INT NOT NULL PRIMARY KEY IDENTITY,
	codigoProveedor INT NOT NULL,
	codigoProducto INT NOT NULL,
	direccion VARCHAR(128),
	precioUnitario DECIMAL(10,2) DEFAULT 0.00,
	cantidad INT NOT NULL,
	total DECIMAL(10,2) DEFAULT 0.00,
	FOREIGN KEY (codigoProveedor) REFERENCES Proveedores(codigoProveedor)
)
GO

CREATE TABLE Ventas(
	codigoVenta INT NOT NULL PRIMARY KEY IDENTITY,
	fechaVenta DATE NOT NULL,
	iva DECIMAL(10,2) DEFAULT 0.12,
	observacion VARCHAR(128),
	fechaCreacion DATE NOT NULL,
	fechaModificacion DATE,
	codigoCliente INT NOT NULL,
	codigoFormaPago INT NOT NULL,
	codigoTipoProducto INT NOT NULL,
	codigoUsuario INT NOT NULL,
	numeroFactura INT NOT NULL,
	total DECIMAL(10,2)
	FOREIGN KEY (codigoCliente) REFERENCES Clientes(codigoCliente),
	FOREIGN KEY (codigoFormaPago) REFERENCES FormaPago(codigoFormaPago),
	FOREIGN KEY (codigoTipoProducto) REFERENCES TipoProducto(codigoTipoProducto),
	FOREIGN KEY (codigoUsuario) REFERENCES Usuarios(codigoUsuario),
)
GO

CREATE TABLE DetalleVentas(
	codigoDetalleVenta INT NOT NULL PRIMARY KEY IDENTITY,
	cantidad INT NOT NULL,
	precioVenta DECIMAL(10,2) DEFAULT 0.00,
	productoIVA DECIMAL(10,2) DEFAULT 0.12,
	codigoVenta INT NOT NULL,
	codigoProducto INT NOT NULL,
	FOREIGN KEY (codigoVenta) REFERENCES Ventas(codigoVenta),
	FOREIGN KEY (codigoProducto) REFERENCES Productos(codigoProducto)
)
GO