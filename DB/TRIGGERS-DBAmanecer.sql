USE DBAmanecer2016129
GO

--TRIGGERS--

--EXISTENCIA DE COMPRAS A PRODUCTOS--
CREATE TRIGGER TR_Compras_Productos_Existencia_Insert
ON Compras
FOR INSERT
AS
	DECLARE @existencia AS INT
	SET @existencia = (SELECT SUM(C.cantidad) FROM Compras C JOIN inserted I ON C.codigoProducto = I.codigoProducto)
	UPDATE Productos SET existencia = @existencia FROM Productos P JOIN inserted I
	ON P.codigoProducto = I.codigoProducto
GO
--
CREATE TRIGGER TR_Compras_Productos_Existencia_Update
ON Compras
FOR UPDATE
AS
	UPDATE Productos SET existencia = (existencia - D.cantidad) FROM Productos P JOIN deleted D
	ON P.codigoProducto = D.codigoProducto
	DECLARE @existencia AS INT
	SET @existencia = (SELECT SUM(C.cantidad) FROM Compras C JOIN inserted I ON C.codigoProducto = I.codigoProducto)
	UPDATE Productos SET existencia = @existencia FROM Productos P JOIN inserted I
	ON P.codigoProducto = I.codigoProducto
GO
--
CREATE TRIGGER TR_Compras_Productos_Existencia_Delete
ON Compras
FOR DELETE
AS
	UPDATE Productos SET existencia = (existencia - D.cantidad) FROM Productos P JOIN deleted D
	ON P.codigoProducto = D.codigoProducto
GO

--PRECIO COSTO DE COMPRAS A PRODUCTOS--
CREATE TRIGGER TR_Compras_Productos_PrecioCosto_Insert
ON Compras
FOR INSERT
AS
	DECLARE @sumaPrecios AS DECIMAL(10,2)
	DECLARE @sumaCompras AS INT
	DECLARE @codigoProducto AS INT
	SET @codigoProducto = (SELECT codigoProducto FROM inserted)
	SET @sumaPrecios = (SELECT SUM(C.precioUnitario) FROM Compras C WHERE C.codigoProducto = @codigoProducto)
	SET @sumaCompras = (SELECT COUNT(C.codigoProducto) FROM Compras C WHERE C.codigoProducto = @codigoProducto)
	UPDATE Productos SET precioCosto = (@sumaPrecios / @sumaCompras) WHERE codigoProducto = @codigoProducto
GO
--
CREATE TRIGGER TR_Compras_Productos_PrecioCosto_Update
ON Compras
FOR UPDATE
AS
	DECLARE @sumaPrecios AS DECIMAL(10,2)
	DECLARE @sumaCompras AS INT
	DECLARE @codigoProducto AS INT
	SET @codigoProducto = (SELECT codigoProducto FROM deleted)
	SET @sumaPrecios = (SELECT SUM(C.precioUnitario) FROM Compras C WHERE C.codigoProducto = @codigoProducto)
	SET @sumaCompras = (SELECT COUNT(C.codigoProducto) FROM Compras C WHERE C.codigoProducto = @codigoProducto)
	IF(@sumaCompras = 0)
	BEGIN
		UPDATE Productos SET precioCosto = 0 WHERE codigoProducto = @codigoProducto
	END
	IF(@sumaCompras > 0)
	BEGIN
		UPDATE Productos SET precioCosto = (@sumaPrecios / @sumaCompras) WHERE codigoProducto = @codigoProducto
	END
	SET @codigoProducto = (SELECT codigoProducto FROM inserted)
	SET @sumaPrecios = (SELECT SUM(C.precioUnitario) FROM Compras C WHERE C.codigoProducto = @codigoProducto)
	SET @sumaCompras = (SELECT COUNT(C.codigoProducto) FROM Compras C WHERE C.codigoProducto = @codigoProducto)
	UPDATE Productos SET precioCosto = (@sumaPrecios / @sumaCompras) WHERE codigoProducto = @codigoProducto
GO
--
CREATE TRIGGER TR_Compras_Productos_PrecioCosto_Delete
ON Compras
FOR DELETE
AS
	DECLARE @sumaPrecios AS DECIMAL(10,2)
	DECLARE @sumaCompras AS INT
	DECLARE @codigoProducto AS INT
	SET @codigoProducto = (SELECT codigoProducto FROM deleted)
	SET @sumaPrecios = (SELECT SUM(C.precioUnitario) FROM Compras C WHERE C.codigoProducto = @codigoProducto)
	SET @sumaCompras = (SELECT COUNT(C.codigoProducto) FROM Compras C WHERE C.codigoProducto = @codigoProducto)
	IF(@sumaCompras = 0)
	BEGIN
		UPDATE Productos SET precioCosto = 0 WHERE codigoProducto = @codigoProducto
	END
	IF(@sumaCompras > 0)
	BEGIN
		UPDATE Productos SET precioCosto = (@sumaPrecios / @sumaCompras) WHERE codigoProducto = @codigoProducto
	END
GO

--PRECIO VENTA E IVA DE PRODUCTOS--
CREATE TRIGGER TR_Productos_PrecioVenta_Update
ON Productos
FOR UPDATE
AS
	DECLARE @codigoProducto AS INT
	DECLARE @precioCosto AS DECIMAL
	SET @codigoProducto = (SELECT codigoProducto FROM inserted)
	SET @precioCosto =(SELECT precioCosto FROM inserted)
	UPDATE Productos SET precioVenta = (@precioCosto * 1.25), productoIva = (@precioCosto * 0.12) WHERE codigoProducto = @CodigoProducto
GO

--EXISTENCIA DE DETALLE DE VENTA A PRODUCTOS--
CREATE TRIGGER TR_DetallesVenta_Productos_Existencia_Insert
ON DetalleVentas
FOR INSERT
AS
	DECLARE @cantidad AS INT
	DECLARE @codigoProducto AS INT
	SET @codigoProducto = (SELECT codigoProducto FROM inserted)
	SET @cantidad = (SELECT cantidad FROM inserted)
	UPDATE Productos SET existencia = (existencia - @cantidad) WHERE codigoProducto = @codigoProducto
GO
--
CREATE TRIGGER TR_DetallesVenta_Productos_Existencia_Delete
ON DetalleVentas
FOR DELETE
AS
	DECLARE @cantidad AS INT
	DECLARE @codigoProducto AS INT
	SET @codigoProducto = (SELECT codigoProducto FROM deleted)
	SET @cantidad = (SELECT cantidad FROM deleted)
	UPDATE Productos SET existencia = (existencia + @cantidad) WHERE codigoProducto = @codigoProducto
GO
--
CREATE TRIGGER TR_DetallesVenta_Productos_Existencia_Update
ON DetalleVentas
FOR UPDATE
AS
	DECLARE @cantidad AS INT
	DECLARE @codigoProducto AS INT
	SET @codigoProducto = (SELECT codigoProducto FROM deleted)
	SET @cantidad = (SELECT cantidad FROM deleted)
	UPDATE Productos SET existencia = (existencia + @cantidad) WHERE codigoProducto = @codigoProducto
	SET @codigoProducto = (SELECT codigoProducto FROM inserted)
	SET @cantidad = (SELECT cantidad FROM inserted)
	UPDATE Productos SET existencia = (existencia - @cantidad) WHERE codigoProducto = @codigoProducto
GO

--TOTAL DE VENTA DE DETALLES DE VENTA A VENTAS--
CREATE TRIGGER TR_DetallesVenta_Ventas_Total_Insert
ON DetalleVentas
FOR INSERT
AS
	DECLARE @total AS DECIMAL(10,2)
	DECLARE @codigoVenta AS INT
	SET @codigoVenta = (SELECT codigoVenta FROM inserted)
	SET @total = (SELECT SUM(cantidad * precioVenta) FROM DetalleVentas WHERE codigoVenta = @codigoVenta)
	UPDATE Ventas SET total = @total WHERE codigoVenta = @codigoVenta
GO
--
CREATE TRIGGER TR_DetallesVenta_Ventas_Total_Update
ON DetalleVentas
FOR UPDATE
AS
	DECLARE @total AS DECIMAL(10,2)
	DECLARE @codigoVenta AS INT
	SET @codigoVenta = (SELECT codigoVenta FROM inserted)
	SET @total = (SELECT SUM(cantidad * precioVenta) FROM DetalleVentas WHERE codigoVenta = @codigoVenta)
	UPDATE Ventas SET total = @total WHERE codigoVenta = @codigoVenta
GO
--
CREATE TRIGGER TR_DetallesVenta_Ventas_Total_Delete
ON DetalleVentas
FOR DELETE
AS
	DECLARE @total AS DECIMAL(10,2)
	DECLARE @codigoVenta AS INT
	SET @codigoVenta = (SELECT codigoVenta FROM deleted)
	SET @total = (SELECT SUM(cantidad * precioVenta) FROM DetalleVentas WHERE codigoVenta = @codigoVenta)
	UPDATE Ventas SET total = @total WHERE codigoVenta = @codigoVenta
GO

--NUMERO DE FACTURA DE VENTAS A FACTURA--
CREATE TRIGGER TR_Ventas_Facturas_NumeroFactura_Update
ON Ventas
FOR UPDATE
AS
	DECLARE @numeroFacturaInserted AS INT
	DECLARE @numeroFacturaDeleted AS INT
	SET @numeroFacturaInserted = (SELECT numeroFactura from inserted)
	SET @numeroFacturaDeleted = (SELECT numeroFactura from deleted)
	UPDATE Facturas SET numeroFactura = @numeroFacturaInserted WHERE numeroFactura = @numeroFacturaDeleted
GO