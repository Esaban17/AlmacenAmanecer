USE DBAmanecer2016129
GO
----------------------------------------------------------------------------------
CREATE PROCEDURE SP_InsertarUsuarios
	@nombreUsuario VARCHAR(50),
	@apellidoUsuario VARCHAR(50),
	@login VARCHAR(128),
	@contrasena VARCHAR(256)
AS
BEGIN
	INSERT INTO Usuarios(nombreUsuario,apellidoUsuario,login,contrasena)
	VALUES (@nombreUsuario,@apellidoUsuario,@login,@contrasena)
END
GO
----------------------------------------------------------------------------------

CREATE PROCEDURE SP_ModificarUsuarios 
	@codigoUsuario INT,
	@nombreUsuario VARCHAR(50),
	@apellidoUsuario VARCHAR(50),
	@login VARCHAR(128),
	@contrasena VARCHAR(256)
AS
BEGIN
	UPDATE Usuarios
	SET nombreUsuario = @nombreUsuario,
		apellidoUsuario = @apellidoUsuario,
		login = @login,
		contrasena = @contrasena
	WHERE codigoUsuario = @codigoUsuario 
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_EliminarUsuarios
	@codigoUsuario INT
AS
BEGIN 
	DELETE FROM Usuarios WHERE codigoUsuario = @codigoUsuario
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_ConsultarUsuarios
AS
BEGIN 
	SELECT * FROM Usuarios
END
GO
--------------------------------------------------------------------------------

CREATE PROCEDURE SP_BuscarUsuarios
	@login VARCHAR(128)
AS
BEGIN
	SELECT * FROM Usuarios WHERE login = @login
END
GO
--------------------------------------------------------------------------------