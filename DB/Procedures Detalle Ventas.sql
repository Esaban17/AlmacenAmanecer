USE DBAmanecer2016129
GO
--DETALLES VENTA-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--INSERT--
CREATE PROCEDURE SP_InsertarDetalleVentas
@cantidad INT,
@precioVenta DECIMAL(10,2),
@productoIva DECIMAL(10,2),
@codigoVenta INT,
@codigoProducto INT,
@numeroFactura INT,
@fecha DATE,
@nitCliente VARCHAR(128),
@total DECIMAL(10,2),
@codigoCliente INT
AS
BEGIN
	INSERT INTO DetalleVentas VALUES (@cantidad, @precioVenta, @productoIva, @codigoVenta, @codigoProducto)
	INSERT INTO Factura VALUES (@numeroFactura, @fecha, @nitCliente, @cantidad, @precioVenta, @total, @codigoProducto, @codigoCliente)
END
GO
--UPDATE--
CREATE PROCEDURE SP_ModificarDetalleVentas
@cantidad INT,
@precioVenta DECIMAL(10,2),
@productoIva DECIMAL(10,2),
@codigoVenta INT,
@codigoProducto INT,
@codigoDetalleVenta INT,
@numeroFactura INT,
@fecha DATE,
@nitCliente VARCHAR(128),
@total DECIMAL(10,2),
@codigoCliente INT,
@codigoFactura INT
AS
BEGIN
	UPDATE DetalleVentas SET cantidad = @cantidad, precioVenta = @precioVenta, productoIva = @productoIva, codigoVenta = @codigoVenta,
								codigoProducto = @codigoProducto WHERE codigoDetalleVenta = @codigoDetalleVenta
	UPDATE Factura SET numeroFactura = @numeroFactura, fecha = @fecha, nitCliente = @nitCliente, cantidad = @cantidad, precioVenta = @precioVenta,
						total = @total, codigoProducto = @codigoProducto, codigoCliente = @codigoCliente WHERE codigoFactura = @codigoFactura
END
GO
--DELETE--
CREATE PROCEDURE SP_EliminarDetalleVentas (@codigoDetalleVenta INT, @codigoFactura INT)
AS
BEGIN
	DELETE DetalleVentas WHERE codigoDetalleVenta = @codigoDetalleVenta
	DELETE Factura WHERE codigoFactura = @codigoFactura
END
GO
--LIST--
CREATE PROCEDURE SP_ConsultarDetalleVentas (@codigoVenta INT)
AS
BEGIN
	SELECT * FROM DetalleVentas WHERE codigoVenta = @codigoVenta
END
GO
