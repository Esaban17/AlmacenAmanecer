USE DBAmanecer2016129
GO
----------------------------------------------------------------------------------
CREATE PROCEDURE SP_InsertarTelefonoProveedor
	@numero VARCHAR(8),
	@descripcion VARCHAR(30),
	@codigoProveedor INT
AS
BEGIN
	INSERT INTO TelefonoProveedor(numero,descripcion,codigoProveedor)
	VALUES (@numero,@descripcion,@codigoProveedor)
END
GO
----------------------------------------------------------------------------------

CREATE PROCEDURE SP_ModificarTelefonoProveedor 
	@codigoTelefonoProveedor INT,
	@numero VARCHAR(8),
	@descripcion VARCHAR(30),
	@codigoProveedor INT
AS
BEGIN
	UPDATE TelefonoProveedor
	SET numero = @numero,
		descripcion = @descripcion,
		codigoProveedor = @codigoProveedor
	WHERE codigoTelefonoProveedor = @codigoTelefonoProveedor 
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_EliminarTelefonoProveedor
	@codigoTelefonoProveedor INT
AS
BEGIN 
	DELETE FROM TelefonoProveedor WHERE codigoTelefonoProveedor = @codigoTelefonoProveedor
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_ConsultarTelefonoProveedor
AS
BEGIN 
	SELECT * FROM TelefonoProveedor
END
GO
--------------------------------------------------------------------------------

CREATE PROCEDURE SP_BuscarTelefonoProveedor
	@codigoTelefonoProveedor INT
AS
BEGIN
	SELECT * FROM TelefonoProveedor WHERE codigoTelefonoProveedor = @codigoTelefonoProveedor
END
GO
--------------------------------------------------------------------------------