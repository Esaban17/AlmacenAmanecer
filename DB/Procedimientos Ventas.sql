USE DBAmanecer2016129
GO
----------------------------------------------------------------------------------
CREATE PROCEDURE SP_InsertarVentas
	@fechaVenta DATE,
	@iva DECIMAL(10,2),
	@observacion VARCHAR(128),
	@fechaCreacion DATE,
	@codigoCliente INT,
	@codigoFormaPago INT,
	@codigoTipoProducto INT,
	@codigoUsuario INT,
	@numeroFactura INT
AS
BEGIN
	INSERT INTO Ventas(fechaVenta,iva,observacion,fechaCreacion,codigoCliente,codigoFormaPago,codigoTipoProducto,codigoUsuario,numeroFactura)
	VALUES (@fechaVenta,@iva,@observacion,@fechaCreacion,@codigoCliente,@codigoFormaPago,@codigoTipoProducto,@codigoUsuario,@numeroFactura)
END
GO
----------------------------------------------------------------------------------

CREATE PROCEDURE SP_ModificarVentas 
	@codigoVenta INT,
	@fechaVenta DATE,
	@iva DECIMAL(10,2),
	@observacion VARCHAR(128),
	@fechaModificacion DATE,
	@codigoCliente INT,
	@codigoFormaPago INT,
	@codigoTipoProducto INT,
	@codigoUsuario INT,
	@numeroFactura INT
AS
BEGIN
	UPDATE Ventas
	SET fechaVenta = @fechaVenta,
		iva = @iva,
		observacion = @observacion,
		fechaModificacion = @fechaModificacion,
		codigoCliente = @codigoCliente,
		codigoFormaPago = @codigoFormaPago,
		codigoTipoProducto = @codigoTipoProducto,
		codigoUsuario = @codigoUsuario,
		numeroFactura = @numeroFactura
	WHERE codigoVenta = @codigoVenta 
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_EliminarVentas
	@codigoVenta INT
AS
BEGIN 
	DELETE FROM Ventas WHERE codigoVenta = @codigoVenta
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_ConsultarVentas
AS
BEGIN 
	SELECT * FROM Ventas
END
GO
--------------------------------------------------------------------------------

CREATE PROCEDURE SP_BuscarVentas
	@codigoVenta INT
AS
BEGIN
	SELECT * FROM Ventas WHERE codigoVenta = @codigoVenta
END
GO
--------------------------------------------------------------------------------