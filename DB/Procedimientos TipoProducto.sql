USE DBAmanecer2016129
GO
----------------------------------------------------------------------------------
CREATE PROCEDURE SP_InsertarTipoProducto
	@descripcion VARCHAR(128),
	@fechaCreacion DATE
AS
BEGIN
	INSERT INTO TipoProducto(descripcion,fechaCreacion)
	VALUES (@descripcion,@fechaCreacion)
END
GO
----------------------------------------------------------------------------------

CREATE PROCEDURE SP_ModificarTipoProducto
	@codigoTipoProducto INT,
	@descripcion VARCHAR(128),
	@fechaModificacion DATE
AS
BEGIN
	UPDATE TipoProducto
	SET descripcion = @descripcion,
		fechaModificacion = @fechaModificacion
	WHERE codigoTipoProducto = @codigoTipoProducto 
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_EliminarTipoProducto
	@codigoTipoProducto INT
AS
BEGIN 
	DELETE FROM TipoProducto WHERE codigoTipoProducto = @codigoTipoProducto
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_ConsultarTipoProducto
AS
BEGIN 
	SELECT * FROM TipoProducto
END
GO
--------------------------------------------------------------------------------

CREATE PROCEDURE SP_BuscarTipoProducto
	@codigoTipoProducto INT
AS
BEGIN
	SELECT * FROM TipoProducto WHERE codigoTipoProducto = @codigoTipoProducto
END
GO
--------------------------------------------------------------------------------