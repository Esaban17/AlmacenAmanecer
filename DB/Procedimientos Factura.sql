USE DBAmanecer2016129
GO
-----------------------------------------------------------------
CREATE PROCEDURE SP_InsertarFactura
	@numeroFactura INT,
	@fecha DATE,
	@nitCliente VARCHAR(128),
	@cantidad INT,
	@precioVenta DECIMAL(10,2),
	@codigoProducto INT,
	@total DECIMAL(10,2),
	@codigoCliente INT
AS
BEGIN
	INSERT INTO Factura(numeroFactura,fecha,nitCliente,
						  cantidad,precioVenta,codigoProducto,
						  total,codigoCliente)
	VALUES (@numeroFactura,@fecha,@nitCliente,
			@cantidad,@precioVenta,@codigoProducto,
			@total,@codigoCliente)
END
GO
----------------------------------------------------------------

CREATE PROCEDURE SP_ModificarFactura
	@codigoFactura INT,
	@numeroFactura INT,
	@fecha DATE,
	@nitCliente VARCHAR(128),
	@cantidad INT,
	@precioVenta DECIMAL(10,2),
	@codigoProducto INT,
	@total DECIMAL(10,2),
	@codigoCliente INT
AS
BEGIN
	UPDATE Factura
	SET numeroFactura = @numeroFactura,
		fecha = @fecha,
		nitCliente = @nitCliente,
		cantidad = @cantidad,
		precioVenta = @precioVenta,
		codigoProducto = @codigoProducto,
		total = @total,
		codigoCliente = @codigoCliente
	WHERE codigoFactura = @codigoFactura 
END
GO
------------------------------------------------------------------------

CREATE PROCEDURE SP_EliminarFactura
	@codigoFactura INT
AS
BEGIN 
	DELETE FROM Factura WHERE codigoFactura = @codigoFactura
END
GO

-------------------------------------------------------------------------

CREATE PROCEDURE SP_ConsultarFactura
AS
BEGIN 
	SELECT * FROM Factura
END
GO
-------------------------------------------------------------------------

CREATE PROCEDURE SP_ConsultarPrecio
	@CodigoProducto INT
AS
BEGIN 
	SELECT precioVenta FROM Productos WHERE codigoProducto = @CodigoProducto
END
GO
-------------------------------------------------------------------------

CREATE PROCEDURE SP_ConsultarNit
	@CodigoCliente INT
AS
BEGIN 
	SELECT nitCliente FROM Clientes WHERE codigoCliente = @CodigoCliente
END
GO
-------------------------------------------------------------------------

CREATE PROCEDURE SP_BuscarFactura
	@codigoFactura INT
AS
BEGIN
	SELECT * FROM Factura WHERE codigoFactura = @codigoFactura
END
GO
-------------------------------------------------------------------------