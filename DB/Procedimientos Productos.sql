USE DBAmanecer2016129
GO
----------------------------------------------------------------------------------
CREATE PROCEDURE SP_InsertarProductos
	@nombreProducto VARCHAR(128),
	@existencia INT,
	@precioCosto DECIMAL(10,2),
	@precioVenta DECIMAL(10,2),
	@productoIVA DECIMAL(10,2),
	@fechaCreacion DATE,
	@codigoTipoProducto INT,
	@codigoProveedor INT
AS
BEGIN
	INSERT INTO Productos(nombreProducto,existencia,precioCosto,
						  precioVenta,productoIVA,fechaCreacion,codigoTipoProducto,codigoProveedor)
	VALUES (@nombreProducto,@existencia,@precioCosto,
			@precioVenta,@productoIVA,@fechaCreacion,@codigoTipoProducto,@codigoProveedor)
END
GO
----------------------------------------------------------------------------------

CREATE PROCEDURE SP_ModificarProductos 
	@codigoProducto INT,
	@nombreProducto VARCHAR(128),
	@existencia INT,
	@precioCosto DECIMAL(10,2),
	@precioVenta DECIMAL(10,2),
	@productoIVA DECIMAL(10,2),
	@fechaModificacion DATE,
	@codigoTipoProducto INT,
	@codigoProveedor INT
AS
BEGIN
	UPDATE Productos
	SET nombreProducto = @nombreProducto,
		existencia = @existencia,
		precioCosto = @precioCosto,
		precioVenta = @precioVenta,
		productoIVA = @productoIVA,
		fechaModificacion = @fechaModificacion,
		codigoTipoProducto = @codigoTipoProducto,
		codigoProveedor = @codigoProveedor
	WHERE codigoProducto = @codigoProducto 
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_EliminarProductos
	@codigoProducto INT
AS
BEGIN 
	DELETE FROM Productos WHERE codigoProducto = @codigoProducto
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_ConsultarProductos
AS
BEGIN 
	SELECT * FROM Productos
END
GO
--------------------------------------------------------------------------------

CREATE PROCEDURE SP_BuscarProductos
	@codigoProducto INT
AS
BEGIN
	SELECT * FROM Productos WHERE codigoProducto = @codigoProducto
END
GO
--------------------------------------------------------------------------------