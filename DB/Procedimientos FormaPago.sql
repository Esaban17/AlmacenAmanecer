USE DBAmanecer2016129
GO
----------------------------------------------------------------------------------
CREATE PROCEDURE SP_InsertarFormaPago
	@descripcion VARCHAR(128)
AS
BEGIN
	INSERT INTO FormaPago(descripcion)
	VALUES (@descripcion)
END
GO

EXECUTE SP_InsertarFormaPago 'Efectivo'
EXECUTE SP_InsertarFormaPago 'Tarjeta de Credito'
EXECUTE SP_InsertarFormaPago 'Tarjeta de Debito'
EXECUTE SP_InsertarFormaPago 'Nota de Credito'
----------------------------------------------------------------------------------

CREATE PROCEDURE SP_ModificarFormaPago
	@codigoFormaPago INT,
	@descripcion VARCHAR(128)
AS
BEGIN
	UPDATE FormaPago
	SET descripcion = @descripcion
	WHERE codigoFormaPago = @codigoFormaPago 
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_EliminarFormaPago
	@codigoFormaPago INT
AS
BEGIN 
	DELETE FROM FormaPago WHERE codigoFormaPago = @codigoFormaPago
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_ConsultarFormaPago
AS
BEGIN 
	SELECT * FROM FormaPago
END
GO
--------------------------------------------------------------------------------

CREATE PROCEDURE SP_BuscarFormaPago
	@codigoFormaPago INT
AS
BEGIN
	SELECT * FROM FormaPago WHERE codigoFormaPago = @codigoFormaPago
END
GO
--------------------------------------------------------------------------------