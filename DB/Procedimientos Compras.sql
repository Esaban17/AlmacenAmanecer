USE DBAmanecer2016129
	GO
----------------------------------------------------------------------------------
CREATE PROCEDURE SP_InsertarCompras
	@codigoProveedor INT,
	@codigoProducto INT,
	@direccion VARCHAR(128),
	@precioUnitario DECIMAL(10,2),
	@cantidad INT,
	@total DECIMAL(10,2)
AS
BEGIN
	INSERT INTO Compras(codigoProveedor,codigoProducto,direccion,precioUnitario,cantidad,total)
	VALUES (@codigoProveedor,@codigoProducto,@direccion,@precioUnitario,@cantidad,@total)
END
GO

----------------------------------------------------------------------------------

CREATE PROCEDURE SP_ModificarCompras
	@codigoCompra INT,
	@codigoProveedor INT,
	@codigoProducto INT,
	@direccion VARCHAR(128),
	@precioUnitario DECIMAL(10,2),
	@cantidad INT,
	@total DECIMAL(10,2)
AS
BEGIN
	UPDATE Compras
	SET codigoProveedor = @codigoProveedor,
		codigoProducto = @codigoProducto,
		direccion = @direccion,
		precioUnitario = @precioUnitario,
		cantidad = @cantidad,
		total = @total
	WHERE codigoCompra = @codigoCompra 
END
GO

---------------------------------------------------------------------------------

CREATE PROCEDURE SP_EliminarCompras
	@codigoCompra INT
AS
BEGIN 
	DELETE FROM Compras WHERE codigoCompra = @codigoCompra
END
GO

---------------------------------------------------------------------------------

CREATE PROCEDURE SP_ConsultarCompras
AS
BEGIN 
	SELECT * FROM Compras
END
GO

--------------------------------------------------------------------------------

CREATE PROCEDURE SP_BuscarCompras
	@codigoCompra INT
AS
BEGIN
	SELECT * FROM Compras WHERE codigoCompra = @codigoCompra
END
GO

--------------------------------------------------------------------------------
CREATE PROCEDURE SP_ConsultarExistencia
@CodigoProducto int
AS
	BEGIN
		SELECT existencia  FROM Productos WHERE Productos.CodigoProducto=@CodigoProducto
END
GO
--------------------------------------------------------------------------------
CREATE PROCEDURE SP_ConsultarPrecioUnitario
@CodigoProducto int
AS
	BEGIN
		SELECT precioVenta  FROM Productos WHERE Productos.CodigoProducto=@CodigoProducto
END
GO
--------------------------------------------------------------------------------