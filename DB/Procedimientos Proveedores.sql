USE DBAmanecer2016129
GO
----------------------------------------------------------------------------------
CREATE PROCEDURE SP_InsertarProveedores
	@razonSocial VARCHAR(128),
	@nit VARCHAR(12),
	@direccionProveedor VARCHAR(128),
	@paginaWeb VARCHAR(200)
AS
BEGIN
	INSERT INTO Proveedores(razonSocial,nit,direccionProveedor,paginaWeb)
	VALUES (@razonSocial,@nit,@direccionProveedor,@paginaWeb)
END
GO
----------------------------------------------------------------------------------

CREATE PROCEDURE SP_ModificarProveedores 
	@codigoProveedor INT,
	@razonSocial VARCHAR(128),
	@nit VARCHAR(12),
	@direccionProveedor VARCHAR(128),
	@paginaWeb VARCHAR(200)
AS
BEGIN
	UPDATE Proveedores
	SET razonSocial = @razonSocial,
		nit = @nit,
		direccionProveedor = @direccionProveedor,
		paginaWeb = @paginaWeb
	WHERE codigoProveedor = @codigoProveedor 
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_EliminarProveedores
	@codigoProveedor INT
AS
BEGIN 
	DELETE FROM Proveedores WHERE codigoProveedor = @codigoProveedor
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_ConsultarProveedores
AS
BEGIN 
	SELECT * FROM Proveedores
END
GO
--------------------------------------------------------------------------------

CREATE PROCEDURE SP_BuscarProveedores
	@codigoProveedor INT
AS
BEGIN
	SELECT * FROM Proveedores WHERE codigoProveedor = @codigoProveedor
END
GO
--------------------------------------------------------------------------------