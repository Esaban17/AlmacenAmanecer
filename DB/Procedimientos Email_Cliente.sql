USE DBAmanecer2016129
GO
----------------------------------------------------------------------------------
CREATE PROCEDURE SP_InsertarEmail_Cliente
	@email VARCHAR(256),
	@descripcion VARCHAR(128),
	@codigoCliente INT
AS
BEGIN
	INSERT INTO Email_Cliente(email,descripcion,codigoCliente)
	VALUES (@email,@descripcion,@codigoCliente)
END
GO

----------------------------------------------------------------------------------

CREATE PROCEDURE SP_ModificarEmail_Cliente 
	@codigoEmailCliente INT,
	@email VARCHAR(256),
	@descripcion VARCHAR(128),
	@codigoCliente INT
AS
BEGIN
	UPDATE Email_Cliente
	SET email = @email,
		descripcion = @descripcion,
		codigoCliente = @codigoCliente
	WHERE codigoEmailCliente = @codigoEmailCliente 
END
GO

---------------------------------------------------------------------------------

CREATE PROCEDURE SP_EliminarEmail_Cliente
	@codigoEmailCliente INT
AS
BEGIN 
	DELETE FROM Email_Cliente WHERE codigoEmailCliente = @codigoEmailCliente
END
GO

---------------------------------------------------------------------------------

CREATE PROCEDURE SP_ConsultarEmail_Cliente
AS
BEGIN 
	SELECT * FROM Email_Cliente
END
GO

--------------------------------------------------------------------------------

CREATE PROCEDURE SP_BuscarEmail_Cliente
	@codigoEmailCliente INT
AS
BEGIN
	SELECT * FROM Email_Cliente WHERE codigoEmailCliente = @codigoEmailCliente
END
GO

--------------------------------------------------------------------------------