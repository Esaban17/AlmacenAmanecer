USE DBAmanecer2016129
GO
----------------------------------------------------------------------------------
CREATE PROCEDURE SP_InsertarTelefonoCliente
	@telefono VARCHAR(8),
	@descripcion VARCHAR(25),
	@codigoCliente INT
AS
BEGIN
	INSERT INTO TelefonoCliente(telefono,descripcion,codigoCliente)
	VALUES (@telefono,@descripcion,@codigoCliente)
END
GO
----------------------------------------------------------------------------------

CREATE PROCEDURE SP_ModificarTelefonoCliente
	@codigoTelefonoCliente INT,
	@telefono VARCHAR(8),
	@descripcion VARCHAR(25),
	@codigoCliente INT
AS
BEGIN
	UPDATE TelefonoCliente
	SET telefono = @telefono,
		descripcion = @descripcion,
		codigoCliente = @codigoCliente
	WHERE codigoTelefonoCliente = @codigoTelefonoCliente 
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_EliminarTelefonoCliente
	@codigoTelefonoCliente INT
AS
BEGIN 
	DELETE FROM TelefonoCliente WHERE codigoTelefonoCliente = @codigoTelefonoCliente
END
GO
---------------------------------------------------------------------------------

CREATE PROCEDURE SP_ConsultarTelefonoCliente
AS
BEGIN 
	SELECT * FROM TelefonoCliente
END
GO
--------------------------------------------------------------------------------

CREATE PROCEDURE SP_BuscarTelefonoCliente
	@codigoTelefonoCliente INT
AS
BEGIN
	SELECT * FROM TelefonoCliente WHERE codigoTelefonoCliente = @codigoTelefonoCliente
END
GO
--------------------------------------------------------------------------------