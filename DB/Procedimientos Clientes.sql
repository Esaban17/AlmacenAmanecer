USE DBAmanecer2016129
GO
----------------------------------------------------------------------------------
CREATE PROCEDURE SP_InsertarClientes
	@nombreCliente VARCHAR(40),
	@apellidoCliente VARCHAR(40),
	@direccionCliente VARCHAR(128),
	@nitCliente VARCHAR(128),
	@fechaCreacion DATE
AS
BEGIN
	INSERT INTO Clientes(nombreCliente,apellidoCliente,direccionCliente,nitCliente,fechaCreacion)
	VALUES (@nombreCliente,@apellidoCliente,@direccionCliente,@nitCliente,@fechaCreacion)
END
GO

----------------------------------------------------------------------------------

CREATE PROCEDURE SP_ModificarClientes 
	@codigoCliente INT,
	@nombreCliente VARCHAR(40),
	@apellidoCliente VARCHAR(40),
	@direccionCliente VARCHAR(128),
	@nitCliente VARCHAR(128),
	@fechaModificacion DATE
AS
BEGIN
	UPDATE Clientes
	SET nombreCliente = @nombreCliente,
		apellidoCliente = @apellidoCliente,
		direccionCliente = @direccionCliente,
		nitCliente = @nitCliente,
		fechaModificacion = @fechaModificacion
	WHERE codigoCliente = @codigoCliente 
END
GO

---------------------------------------------------------------------------------

CREATE PROCEDURE SP_EliminarClientes
	@codigoCliente INT
AS
BEGIN 
	DELETE FROM Clientes WHERE codigoCliente = @codigoCliente
END
GO

---------------------------------------------------------------------------------

CREATE PROCEDURE SP_ConsultarClientes
AS
BEGIN 
	SELECT * FROM Clientes
END
GO
--------------------------------------------------------------------------------

CREATE PROCEDURE SP_BuscarClientes
	@codigoCliente INT
AS
BEGIN
	SELECT * FROM Clientes WHERE codigoCliente = @codigoCliente
END
GO
--------------------------------------------------------------------------------